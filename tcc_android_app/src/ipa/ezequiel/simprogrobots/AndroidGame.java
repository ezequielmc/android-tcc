package ipa.ezequiel.simprogrobots;

import ipa.ezequiel.simprogrobots.framework.Audio;
import ipa.ezequiel.simprogrobots.framework.FileIO;
import ipa.ezequiel.simprogrobots.framework.Game;
import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Input;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.impl.AndroidAudio;
import ipa.ezequiel.simprogrobots.framework.impl.AndroidFastRenderView;
import ipa.ezequiel.simprogrobots.framework.impl.AndroidFileIO;
import ipa.ezequiel.simprogrobots.framework.impl.AndroidGraphics;
import ipa.ezequiel.simprogrobots.framework.impl.AndroidInput;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.Editable;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public abstract class AndroidGame extends Activity 
	implements Game {
    AndroidFastRenderView renderView;
    Graphics graphics;
    Audio audio;
    Input input;
    FileIO fileIO;
    Screen screen;
    WakeLock wakeLock;
    
    /**********************************************/
    // Variaveis das telas
    //TODO: Estudar melhores praticas e cria modelo de testes
    
    ControlScreen cScreen;
    LoadingScreen lScreen;
    ProgramScreen pScreen;
    SendScreen    sScreen;
    SimScreen     simScreen;
    PropertiesScreen propScreen;
    MainMenuRobot mainMenuScreen;
    RobotScreen robotScreen;
    ScenarioScreen scenarioScreen;
	
    boolean isSimulation = true;
    Editable saveFile;
	private Intent intentSimulation;
	
    //Only to GLGame
    public void onCreateActivty(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    }
    
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        
    	super.onCreate(savedInstanceState);
    	
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        boolean isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        
        int frameBufferWidth = isLandscape ? 480 : 320;
        int frameBufferHeight = isLandscape ? 320 : 480;
        
        Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
                frameBufferHeight, Config.RGB_565);
        
		float scaleX = (float) frameBufferWidth
                / getWindowManager().getDefaultDisplay().getWidth();
		float scaleY = (float) frameBufferHeight
                / getWindowManager().getDefaultDisplay().getHeight();

        
		renderView = new AndroidFastRenderView(this, frameBuffer);
        
        graphics = new AndroidGraphics(getAssets(), frameBuffer);
        
        fileIO = (FileIO) new AndroidFileIO(getAssets());
        
        audio = (Audio) new AndroidAudio(this);
        
        input = new AndroidInput(this, renderView, scaleX, scaleY);
        
        setContentView(renderView);
        
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "GLGame");
        
        screen = getStartScreen();
        
    }

    public void initScreens(){
    	//Cria as telas para nao perder historico.
    	mainMenuScreen = new MainMenuRobot(this);
        cScreen = new ControlScreen(this);
        lScreen = new LoadingScreen(this);
        pScreen = new ProgramScreen(this);
        sScreen = new SendScreen(this);
        propScreen = new PropertiesScreen(this);
        robotScreen = new RobotScreen(this);
        scenarioScreen = new ScenarioScreen(this);
        

        try {
			Class<?> clazz = Class.forName("ipa.ezequiel.simprogrobots.Simulation.SimulationScreen" );
			intentSimulation = new Intent(this, clazz);
		}
		catch( ClassNotFoundException e ) {
			e.printStackTrace();
		}
        
        isSimulation = false;
    }
    
    public void onResumeActivity(){
    	super.onResume();
    	BluetoothControler.getBluetooth().disableBluetooth();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        wakeLock.acquire();
        screen.resume();
        renderView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if( this.screen == null )
        	return;
        wakeLock.release();
        renderView.pause();
        screen.pause();

        if (isFinishing())
            screen.dispose();
    }

    @Override
    public Input getInput() {
        return input;
    }

    @Override
    public FileIO getFileIO() {
        return fileIO;
    }

    @Override
    public Graphics getGraphics() {
        return graphics;
    }

    @Override
    public Audio getAudio() {
        return audio;
    }

    @Override
    public void onBackPressed() {
    	
    	System.gc();
    	if( this.screen == null ){
    		super.onBackPressed();
    		return;
    	}

    	if( this.screen.getClass() == PropertiesScreen.class){
    		this.setProgramScreen();
    		return;
    	}
    	
    	if( this.screen.getClass() == MainMenuRobot.class){
    		super.onBackPressed();
    		return;
    	}
    	
    	if( this.screen.getClass() == ControlScreen.class ||
    		this.screen.getClass() == ProgramScreen.class ||
    		this.screen.getClass() == SendScreen.class  ||
    		this.screen.getClass() == RobotScreen.class ||
    		this.screen.getClass() == ScenarioScreen.class ){
        	this.setMainMenu();
        	return;
    	}
    	super.onBackPressed();
    	
    }

    @Override
    public void setScreen(Screen screen) {
        
    	System.gc();
    	//Desliga conexao Bluetooth
    	BluetoothControler.getBluetooth().stopWorker = true;
    	
    	
    	if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        this.screen.dispose();
        screen.resume();
        screen.update(0);
        this.screen = screen;
    }
    
    public void setMainMenu(){
    	this.setScreen(mainMenuScreen);
    }
    
    public void setProgramScreen(){
    	pScreen.initMenu();
    	this.setScreen(pScreen);
    }

    public void setControlScreen(){
    	this.setScreen(cScreen);
    }
    
    public void setSendScreen(){
    	this.setScreen(sScreen);
    }
    
    public void setPropertiesScreen(){
    	this.setScreen(propScreen);
    }
    
    public void setRobotScreen(){
    	this.setScreen(robotScreen);
    }
    
    public void setScenarioScreen(){
    	this.setScreen(scenarioScreen);
    }
    
    public void setIsSimulation( boolean value ){
    	this.isSimulation = value;
    }

    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	menu.add("Complete Program");
    	return true;
    }
    
    public void setSimScreen(){
    	if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        
		startActivity( intentSimulation );

    }
    
    public void setScreenBluetooth() {
        if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        
        try {
			Class<?> clazz = Class.forName("ipa.ezequiel.simprogrobots.BluetoothActivity" );
			Intent intent = new Intent(this, clazz);
			startActivity( intent );
		}
		catch( ClassNotFoundException e ) {
			e.printStackTrace();
		}
        
    }
 
    public void showSaveDialog(){
    	
    	if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        
        try {
			Class<?> clazz = Class.forName("ipa.ezequiel.simprogrobots.SaveProgramActivity" );
			Intent intent = new Intent(this, clazz);
			startActivity( intent );
		}
		catch( ClassNotFoundException e ) {
			e.printStackTrace();
		}
    }

    public void showOpenDialog(){
    	if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        
        try {
			Class<?> clazz = Class.forName("ipa.ezequiel.simprogrobots.LoadProgramActivity" );
			Intent intent = new Intent(this, clazz);
			startActivity( intent );
		}
		catch( ClassNotFoundException e ) {
			e.printStackTrace();
		}
    }
    
    public Screen getCurrentScreen() {
        return screen;
    }
}