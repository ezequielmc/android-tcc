package ipa.ezequiel.simprogrobots;

import ipa.ezequiel.simprogrobots.framework.Pixmap;
import ipa.ezequiel.simprogrobots.framework.gl.TextureRegion;
import android.graphics.Typeface;

public class Assets2D {
    
	//TODO: Add every image and sound used on this software
	public static Pixmap background;
	public static Pixmap backgroundProperties;
	
	public static Pixmap logo;
	
	public static Pixmap txtProgramar;
	public static Pixmap txtSimular;
	public static Pixmap txtEnviar;
	public static Pixmap txtControlar;
	public static Pixmap imgVoltar;

	public static Object clickSound;

	public static TextureRegion backgroundRegion;

	/* Options */
	public static Pixmap imgSave;
	public static Pixmap imgLoad;
	public static Pixmap imgRun;
	public static Pixmap imgGarbage;
	public static Pixmap imgGarbageSelected;
	
	/* Images to Gear Control */
	public static Pixmap imgFront;
	public static Pixmap imgLeft;
	public static Pixmap imgRight;
	public static Pixmap imgBack;
	
	
	public static Pixmap imgIF;
	public static Pixmap imgEndIf;
	
	public static Pixmap imgWHILE;
	public static Pixmap imgEndWhile;
	
	/* Images Control Devices */
	public static Pixmap imgOnBluetooth;
	public static Pixmap imgOffBluetooth;
	
	
	// Images Select Robot
	public static Pixmap imgRobot01;
	public static Pixmap imgRobot02;
	public static Pixmap imgBoxRobot;
	public static Pixmap imgSelectedBoxRobot;
	public static Pixmap imgRobot01Small;
	public static Pixmap imgRobot02Small;
	public static Pixmap imgBoxRobotSmall;
	

	
	// Images Scenario Robot
	public static Pixmap imgScenario01;
	public static Pixmap imgScenario02;
	public static Pixmap imgScenario03;
	public static Pixmap imgSelectedScenario;
	public static Pixmap imgScenario01Small;
	public static Pixmap imgScenario02Small;
	public static Pixmap imgScenario03Small;
	
	
	
	public static Typeface textTypeFace;
	public static Pixmap imgEmptyBox;
	public static Pixmap imgStart;
	public static Pixmap imgD_Dir;
	public static Pixmap imgD_Esq;
	public static Pixmap imgD_Normal;
	public static Pixmap imgLedOff;
	public static Pixmap imgLedOn;
	public static Pixmap imgMessage;
	public static Pixmap imgS_Dist;
	public static Pixmap imgS_Toque;
	public static Pixmap imgTimer;
	public static Pixmap imgBorder;
	public static Pixmap imgPos_S_Dist;
	public static Pixmap imgLoopLeftUp;
	public static Pixmap imgLoopRightUp;
	public static Pixmap imgLoopMidle;
	public static Pixmap imgLoopLeftDown;
	public static Pixmap imgLoopRightDown;
	public static Pixmap imgChecked;
	public static Pixmap imgUnchecked;
	
	
	
}
