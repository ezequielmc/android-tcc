package ipa.ezequiel.simprogrobots;

import java.io.IOException;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class BluetoothActivity extends Activity {
	  private static final int REQUEST_ENABLE_BT = 1;
	   private Button onBtn;
	   private Button offBtn;
	   private Button listBtn;
	   private Button findBtn;
	   private TextView text;
	   private BluetoothAdapter myBluetoothAdapter;
	   private Set<BluetoothDevice> pairedDevices;
	   private ListView myListView;
	   private ArrayAdapter<String> BTArrayAdapter;
	  
	   @Override
	   protected void onCreate(Bundle savedInstanceState) {
	      super.onCreate(savedInstanceState);
	      setContentView(R.layout.activity_bluetooth);
	      
	      // take an instance of BluetoothAdapter - Bluetooth radio
	      myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	      if(myBluetoothAdapter == null) {
	    	  onBtn.setEnabled(false);
	    	  offBtn.setEnabled(false);
	    	  listBtn.setEnabled(false);
	    	  findBtn.setEnabled(false);
	    	  text.setText("Status: not supported");
	    	  
	    	  Toast.makeText(getApplicationContext(),"Your device does not support Bluetooth",
	         		 Toast.LENGTH_LONG).show();
	      } else {
		      text = (TextView) findViewById(R.id.text);
		      onBtn = (Button)findViewById(R.id.turnOn);
		      onBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					on(v);
				}
		      });
		      
		      offBtn = (Button)findViewById(R.id.turnOff);
		      offBtn.setOnClickListener(new OnClickListener() {
		  		
		  		@Override
		  		public void onClick(View v) {
		  			// TODO Auto-generated method stub
		  			off(v);
		  		}
		      });
		      
		      listBtn = (Button)findViewById(R.id.paired);
		      listBtn.setOnClickListener(new OnClickListener() {
		  		
		  		@Override
		  		public void onClick(View v) {
		  			// TODO Auto-generated method stub
		  			list(v);
		  		}
		      });

		      myListView = (ListView)findViewById(R.id.listView1);
		
		      // create the arrayAdapter that contains the BTDevices, and set it to the ListView
		      BTArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		      myListView.setAdapter(BTArrayAdapter);
	      
		      myListView.setOnItemClickListener(new OnItemClickListener() {
		    	  @Override
		    	  public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
		    		  setBluetooth( parent, view, position, id );
		    	  }
   	          });
	      
		      if(myBluetoothAdapter.isEnabled() && BluetoothControler.getBluetooth().isConnected() ) {
				   text.setText("Status: Connectado com " + BluetoothControler.getBluetooth().getDeviceName() );
			  }
		      if(myBluetoothAdapter.isEnabled() ){
		    	  text.setText("Status: Ativado" );
		      }
		      else {   
				  text.setText("Status: Desabilitado");
			   }
	      }
	   }

	   public void setBluetooth( AdapterView<?> parent, View view, int position, long id){
		   
	      pairedDevices = myBluetoothAdapter.getBondedDevices();
		      
	      // put it's one to the adapter
	      Object item = (Object) myListView.getItemAtPosition(position);
	      for(BluetoothDevice device : pairedDevices){
	    	  String str_device = device.getName()+ "\n" + device.getAddress();
	    	  String selected = item.toString();
	    	  if( str_device.compareTo(selected) == 0 ){
	    		  BluetoothControler.getBluetooth().setBluetoothDevice(device);
	    	  }
	      }
		  Toast.makeText(this,"Voce selecionou: " + item.toString(),Toast.LENGTH_SHORT).show();
		  Toast.makeText(this,"Realizando tentativa de conexao.", Toast.LENGTH_SHORT).show();
		  
		  try {
			BluetoothControler.getBluetooth().sendData("!");
			Toast.makeText(this,"Conexao realizada com sucesso.", Toast.LENGTH_SHORT).show();
		  } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(this,"Nao foi possivel realizar a conexao.", Toast.LENGTH_SHORT).show();
			  
		  }
		  
	   }
	   
	   public void on(View view){
	      if (!myBluetoothAdapter.isEnabled()) {
	         Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	         startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);

	         Toast.makeText(getApplicationContext(),"Bluetooth ativo" ,
	        		 Toast.LENGTH_LONG).show();
	      }
	      else{
	         Toast.makeText(getApplicationContext(),"Bluetooth ja estava ativo",
	        		 Toast.LENGTH_LONG).show();
	      }
	   }
	   
	   @Override
	   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		   // TODO Auto-generated method stub
		   if(requestCode == REQUEST_ENABLE_BT){
			  if(myBluetoothAdapter.isEnabled() && BluetoothControler.getBluetooth().isConnected() ) {
				   text.setText("Status: Connectado com " + BluetoothControler.getBluetooth().getDeviceName() );
			  }
		      if(myBluetoothAdapter.isEnabled() ){
		    	  text.setText("Status: Ativado" );
		      }
		      else {   
				  text.setText("Status: Desabilitado");
			  }
		   }
	   }
	   
	   public void list(View view){
		  // get paired devices
	      pairedDevices = myBluetoothAdapter.getBondedDevices();
	      
	      // put it's one to the adapter
	      for(BluetoothDevice device : pairedDevices){
	    	  BTArrayAdapter.add(device.getName()+ "\n" + device.getAddress());
	      }
	      Toast.makeText(getApplicationContext(),"Mostrando dispositivos pairados",
	    		  Toast.LENGTH_SHORT).show();
	      
	   }
	   
	   final BroadcastReceiver bReceiver = new BroadcastReceiver() {
		    public void onReceive(Context context, Intent intent) {
		        String action = intent.getAction();
		        // When discovery finds a device
		        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
		             // Get the BluetoothDevice object from the Intent
		        	 BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		        	 // add the name and the MAC address of the object to the arrayAdapter
		             BTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
		             BTArrayAdapter.notifyDataSetChanged();
		        }
		    }
		};
	

		public void find(View view) {
		   if (myBluetoothAdapter.isDiscovering()) {
			   // the button is pressed when it discovers, so cancel the discovery
			   myBluetoothAdapter.cancelDiscovery();
		   }
		   else {
				BTArrayAdapter.clear();
				myBluetoothAdapter.startDiscovery();
				
				registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));	
			}    
	   }
	   
	   public void off(View view){
		  myBluetoothAdapter.disable();
		  text.setText("Status: Disconnected");
		  
	      Toast.makeText(getApplicationContext(),"Bluetooth turned off",
	    		  Toast.LENGTH_LONG).show();
	   }
	   
	   @Override
	   protected void onDestroy() {
		   // TODO Auto-generated method stub
		   super.onDestroy();
	   }
}