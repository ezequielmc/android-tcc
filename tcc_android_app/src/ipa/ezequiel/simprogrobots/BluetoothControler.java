package ipa.ezequiel.simprogrobots;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.UUID;

import android.R.string;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;


/*** Its a Singleton Class ****/
public final class BluetoothControler {

	private boolean isConnected = false;
	private static BluetoothControler reference;
	private BluetoothDevice selectedDevice;
	private BluetoothSocket mmSocket;
	private OutputStream mmOutputStream;
	private InputStream  mmInputStream;
	
	LinkedList<String> receivedData = new LinkedList<String>();
	public int totalCommands;
    private String inPassCommand = new String();

	Thread workerThread;
	byte[] readBuffer;
	int readBufferPosition;
	int counter;
	volatile boolean stopWorker;
	
	public BluetoothControler(){
		isConnected = false;
	}
	
	public boolean isConnected() {
		return isConnected;
	}
	
	 public static synchronized BluetoothControler getBluetooth()
	 {
	      if (reference == null)
	    	  reference = new BluetoothControler();
	      return reference;
	 }

	public void disableBluetooth() {
		isConnected = false;
	}

	
	public void setBluetoothDevice(BluetoothDevice dev){
		this.selectedDevice = dev;
	}

	public int sendData( String data ) throws IOException{
		if( this.selectedDevice == null )
			return -1;
		else {
			Log.d("Connected Device", "Connecting Device");
			if( !isConnected )
				openDataBluetooth();

			sendDataBluetooth( data );
		}
		return 0;

	}

	
	public void openDataBluetooth() throws IOException {
	    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard //SerialPortService ID
	    mmSocket = this.selectedDevice.createRfcommSocketToServiceRecord(uuid);    
	    mmSocket.connect();
	    mmOutputStream = mmSocket.getOutputStream();
	    mmInputStream = mmSocket.getInputStream();
	    isConnected = true;
	}
	
	
	private void sendDataBluetooth(String data) throws IOException {
		Log.d("Bluetooth", "Try to send data");

		if( isConnected ){
			String msg = data;
			mmOutputStream.write(msg.getBytes());
		}
	}

	public void closeBluetooth() throws IOException {
	    mmOutputStream.close();
	    mmInputStream.close();
	    mmSocket.close();
	    isConnected = false;
	}
	
	public String getDeviceName(){
		return selectedDevice.getName();
	}
	

	void beginListenForData()
	{
		
		stopWorker = false;
	    readBufferPosition = 0;
	    readBuffer = new byte[1024];
	    workerThread = new Thread(new Runnable()
	    {

			public void run()
	        {                
	           while(!Thread.currentThread().isInterrupted() && !stopWorker)
	           {
	        	   try {
	        		   Thread.sleep(500);
	        	   } catch (Exception e) {
	        		Log.d("TAG","Exception Thread.sleep()");
	        	  } 
	        	   
	        	   try 
	                {
	                    int bytesAvailable = mmInputStream.available();                        
	                    if(bytesAvailable > 0)
	                    {
	                        byte[] packetBytes = new byte[bytesAvailable];
	                        mmInputStream.read(packetBytes);
	                        for(int i=0;i<bytesAvailable;i++)
	                        {
	                            char b = (char) packetBytes[i];
	                            Log.d("btData", "valor = " + b);
	                            if( b == '|'){
	                            	if( inPassCommand.contains("SP") ){
	                            		inPassCommand = new String();
	                            		continue;
	                            	}
	                            	receivedData.add(inPassCommand);
	                            	inPassCommand = new String();
	                            }
	                            else
	                            	inPassCommand = inPassCommand + b;
	                            
	                            Log.d("btData", receivedData.toString());
	                            Log.d("btData", inPassCommand);
	                        }
	                    }
	                }
	                catch (IOException ex) 
	                {
	                    stopWorker = true;
	                }
	           }
	        }
	    });
	
	    workerThread.start();
	}
	

}
