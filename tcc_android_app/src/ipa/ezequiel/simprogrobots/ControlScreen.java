package ipa.ezequiel.simprogrobots;

import java.io.IOException;
import java.util.List;

import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;
import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.framework.program.StoreProgram;
import ipa.ezequiel.simprogrobots.Assets2D;

public class ControlScreen extends Screen {
	
	private int posX = 400;
	private int posY = 200;
	private boolean isCircleTouched = false;
	private float totalTimeSend = 0f;
	private boolean isAccelTouched = false;
	private boolean isSensorTouched = false;
	private int posAccell = 10;
	private int posSensor = 0;
	
	
	public ControlScreen(AndroidGame game) {
		super(game);
	}

	
	@Override
	public void update(float deltaTime) {
		//Graphics g = game.getGraphics();
        
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        
    	game.getInput().getKeyEvents();       
        
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if( event.type == TouchEvent.TOUCH_DOWN){
            	
            	if(inBoundsCircle(event,400,200,75)){
            		posX = event.x;
            		posY = event.y;
            		isCircleTouched = true;
            	}
        		
            	else if(inBounds(event, 15, 65, 45, 210)){
            		isAccelTouched = true;
            		posAccell(event);
            	}
            	
            	else if(inBounds(event, 85,135,210,40) && StoreProgram.getStoreProgram().getiRobot() == 1){
            		isSensorTouched = true;
            		posSensor(event);
            	}	
            }
            else if(event.type == TouchEvent.TOUCH_DRAGGED){
            	
            	// Valida posicoes para o Pad
            	if(inBoundsCircle(event,400,200,75) && isCircleTouched){
            		posX = event.x;
            		posY = event.y;
            	}
            	
            	else if(isCircleTouched){
            		int varX = event.x - 400;
            		int varY = event.y - 200;
            		double hipotenusa = Math.sqrt( Math.pow(varX,2) + Math.pow(varY, 2));
            		posX = (int) (( double ) (varX/hipotenusa) * 75 + 400);
            		posY = (int) (( double ) (varY/hipotenusa) * 75 + 200);
            	}
            	else if(isAccelTouched){
            		posAccell(event);
            	}
            	else if(isSensorTouched){
            		posSensor(event);
            	}
            	
            }
            else if(event.type == TouchEvent.TOUCH_UP) {
            	posX = 400;
                posY = 200;
                
            	if(inBounds(event, 10, 0, 74, 64) && !(isAccelTouched | isCircleTouched | isSensorTouched )) {
                	if( BluetoothControler.getBluetooth().isConnected() ){
                		BluetoothControler.getBluetooth().disableBluetooth();
                	}else{
                		game.setScreenBluetooth();
                	}
                	
                }
            	isCircleTouched = false;
                isAccelTouched = false;
                isSensorTouched = false;
                
            }
        }

	}


	private void posAccell(TouchEvent event) {
		float actualPos = - event.y + 270  ;
		float maxPx = 200;
		
		if(actualPos < 0)
			actualPos = 0;
		else if (actualPos > maxPx)
			actualPos = maxPx;
		
		float steps = actualPos / (maxPx / 100 ) ;
        	
		posAccell = (int) (Math.round( steps ) );
	}


	private void posSensor(TouchEvent event) {
		int actualPos = event.x - 90;
		int maxPx = 190;
		if(actualPos < 0)
			actualPos = 0;
		else if (actualPos > maxPx)
			actualPos = maxPx;
		float steps = actualPos / (maxPx / ( 180)) + (-90);
		posSensor = (int) (Math.round( steps ) );
	}

	@Override
	public void present(float deltaTime) {
		Graphics g = game.getGraphics();
		
		g.drawPixmap(Assets2D.background, 0, 0);
		
		totalTimeSend  += deltaTime;
		if(totalTimeSend > 0.5f){
			totalTimeSend = 0f;
			String message = "S";
			if( posX == 350 && posY == 200){
				message = "";
			}
			else{
				if(posX < 300 && posY < 250 && posY > 150){
					message = "L";
				}
				else if(posX > 400  && posY < 250 && posY > 150){
					message = "R";
				}
				else if(posY < 200 ){
					message = "U";
				}
				else if(posY > 200){
					message = "D";
				}
				
				Log.d("Testing Bluetooth", message);
			try {
				BluetoothControler.getBluetooth().sendData(message);
				Log.d("Testing Bluetooth", message);
			} catch (IOException e) {
				Log.d("Error", message);
				Toast.makeText(game.getApplicationContext(), "Nao conectado", Toast.LENGTH_SHORT).show();
			}
				
		}
			
		}

		/*** Draw Control Position GamePad */
		g.drawCircle(400, 200, 75, Color.BLUE);
		g.drawCircle(posX, posY, 10, Color.RED);
		
		
		
		g.drawRect(20, 70, 35, 200, Color.BLUE);
		g.drawRect(13, 70 + ( 195 - (posAccell) * (200 / 100)) ,48, 10, Color.BLACK);
		g.drawText(posAccell + "%", 15, 300, 30, Color.BLACK);
		
		//Pos Sensor
		g.drawRect(90, 140, 200, 30, Color.BLUE);
		g.drawRect(90 + (posSensor + 90) * (200 / (180)), 133, 10, 38, Color.BLACK);
		g.drawText( posSensor + "º", 178, 195, 30, Color.BLACK);
		 
		//Informacoes
		g.drawRect(260, 30, 210, 50, Color.GRAY);
		g.drawText("Informaçoes", 280, 67, 35, Color.BLACK);
		g.drawText("~", 407, 54, 25, Color.BLACK);
				
		if( BluetoothControler.getBluetooth().isConnected()){
			g.drawPixmap(Assets2D.imgOnBluetooth, 10, 0);
			g.drawText("Conectado:"+BluetoothControler.getBluetooth().getDeviceName(), 
					70, 0, 30, Color.BLACK);
		}
		else{
			g.drawPixmap(Assets2D.imgOffBluetooth, 10, 0);
			g.drawText("Não Conectado", 75, 40, 25, Color.BLACK);
		}
		
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
    	if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
    			return true;
        else
                return false;
    }

	private boolean inBoundsCircle(TouchEvent event, int x, int y, int radius) {
		
		int varX = Math.abs(event.x - x);
		int varY = Math.abs(event.y - y);
		int hipotenusa = (int) Math.sqrt( Math.pow(varX,2) + Math.pow(varY, 2));
    	if( hipotenusa <= radius ) 
    			return true;
        else
                return false;
    }

	
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
}
