package ipa.ezequiel.simprogrobots;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class LoadProgramActivity extends Activity {

	private ArrayAdapter<String> FilesArrayAdapter;
	private ListView myListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_program);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		myListView = (ListView)findViewById(ipa.ezequiel.simprogrobots.R.id.listFiles);
		FilesArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1 );
	    myListView.setAdapter(FilesArrayAdapter);
	    
	    //TODO: Alterar
	    FilesArrayAdapter.add("[Programa 01][Cenario 01] Validacao");
	    FilesArrayAdapter.add("[Programa 02][Cenario 02] Teste");
	    FilesArrayAdapter.add("[Programa 03][Cenario 03] Teste");
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.load_program, menu);
		return true;
		
    
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_load_program,
					container, false);
			return rootView;
		}
	}

}
