package ipa.ezequiel.simprogrobots;

import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Graphics.PixmapFormat;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.program.Box;
import ipa.ezequiel.simprogrobots.framework.program.StoreProgram;

import java.util.LinkedList;

public class LoadingScreen extends Screen {
	
	public LoadingScreen( AndroidGame mainSimProgRobot ) {
		super(mainSimProgRobot);
	}

	@Override
	public void update( float deltaTime ){

		/* Cria e inicializa alguns dos itens da primeira tela */
        Graphics g = game.getGraphics();

        Assets2D.background = g.newPixmap("background.png", PixmapFormat.RGB565);
        Assets2D.backgroundProperties = g.newPixmap("wProperties.png", PixmapFormat.RGB565);
        
        Assets2D.logo = g.newPixmap("LogoPNG.png", PixmapFormat.ARGB8888);
        
        Assets2D.txtProgramar = g.newPixmap( "Programar.png", PixmapFormat.RGB565); 
        Assets2D.txtSimular = g.newPixmap( "Simular.png", PixmapFormat.RGB565);
        Assets2D.txtEnviar = g.newPixmap( "Enviar.png", PixmapFormat.RGB565);
        Assets2D.txtControlar = g.newPixmap( "Controlar.png", PixmapFormat.RGB565);
        
        Assets2D.imgVoltar = g.newPixmap("Voltar.png", PixmapFormat.RGB565 );
        
		Settings.load(game.getFileIO());

		//Actions 1.
		Assets2D.imgRun = g.newPixmap("Menu/run.png", PixmapFormat.RGB565 );
		Assets2D.imgLoad = g.newPixmap("Menu/load.png", PixmapFormat.RGB565 );
		Assets2D.imgSave = g.newPixmap("Menu/save.png", PixmapFormat.RGB565 );
		Assets2D.imgGarbage = g.newPixmap("Menu/garbage.png", PixmapFormat.RGB565 );
		Assets2D.imgGarbageSelected = g.newPixmap("Menu/garbageSelected.png", PixmapFormat.RGB565 );
		
		
		/*         Load images to control   */
		Assets2D.imgFront = g.newPixmap("Box/Gear/up.png", PixmapFormat.RGB565 );
		Assets2D.imgLeft = g.newPixmap("Box/Gear/left.png", PixmapFormat.RGB565 );
		Assets2D.imgBack = g.newPixmap("Box/Gear/back.png", PixmapFormat.RGB565 );
		Assets2D.imgRight = g.newPixmap("Box/Gear/right.png", PixmapFormat.RGB565 );
		
		Assets2D.imgIF = g.newPixmap("Menu/Controls/IF.png", PixmapFormat.RGB565 );
		Assets2D.imgEndIf = g.newPixmap("Menu/Controls/EndIF.png", PixmapFormat.RGB565 );
		Assets2D.imgWHILE = g.newPixmap("Menu/Controls/loop.png", PixmapFormat.RGB565 );
		Assets2D.imgEndWhile = g.newPixmap("Menu/Controls/EndLoop.png", PixmapFormat.RGB565 );
		
		
		/*         Load Images to control robot       */
		Assets2D.imgOnBluetooth = g.newPixmap("Menu/Bluetooth/on.png", PixmapFormat.RGB565 );
		Assets2D.imgOffBluetooth = g.newPixmap("Menu/Bluetooth/off.png", PixmapFormat.RGB565 );
		
		
		Assets2D.imgChecked = g.newPixmap("Menu/checked.png", PixmapFormat.RGB565 );
		Assets2D.imgUnchecked = g.newPixmap("Menu/unchecked.png", PixmapFormat.RGB565 );
		
		
		Assets2D.imgRobot01 = g.newPixmap("Robot/Robo01.png", PixmapFormat.RGB565 );
		Assets2D.imgRobot02 = g.newPixmap("Robot/Robo02.png", PixmapFormat.RGB565 );
		Assets2D.imgBoxRobot = g.newPixmap("Robot/Box.png", PixmapFormat.RGB565 );
		Assets2D.imgSelectedBoxRobot = g.newPixmap("Robot/selectedBox.png", PixmapFormat.RGB565 );
		Assets2D.imgRobot01Small = g.newPixmap("Robot/Smaller/Robo01.png", PixmapFormat.RGB565 );
		Assets2D.imgRobot02Small = g.newPixmap("Robot/Smaller/Robo02.png", PixmapFormat.RGB565 );
		Assets2D.imgBoxRobotSmall = g.newPixmap("Robot/Smaller/Box.png", PixmapFormat.RGB565 );
		
		
		Assets2D.imgScenario01 = g.newPixmap("Scenario/scenario01.png", PixmapFormat.RGB565 );
		Assets2D.imgScenario02 = g.newPixmap("Scenario/scenario02.png", PixmapFormat.RGB565 );
		Assets2D.imgScenario03 = g.newPixmap("Scenario/scenario03.png", PixmapFormat.RGB565 );
		Assets2D.imgSelectedScenario = g.newPixmap("Scenario/SelectedBox.png", PixmapFormat.RGB565 );
		Assets2D.imgScenario01Small = g.newPixmap("Scenario/Smaller/scenario01.png", PixmapFormat.RGB565 );
		Assets2D.imgScenario02Small = g.newPixmap("Scenario/Smaller/scenario02.png", PixmapFormat.RGB565 );
		Assets2D.imgScenario03Small = g.newPixmap("Scenario/Smaller/scenario03.png", PixmapFormat.RGB565 );
		
		Assets2D.imgEmptyBox = g.newPixmap("Box/empty_box.png", PixmapFormat.RGB565 );
		Assets2D.imgBorder   = g.newPixmap("Box/border.png",  PixmapFormat.RGB565);
		Assets2D.imgStart = g.newPixmap("Box/start.png", PixmapFormat.RGB565 );
		
		Assets2D.imgD_Dir = g.newPixmap("Box/d_direita.png", PixmapFormat.RGB565 );
		Assets2D.imgD_Esq = g.newPixmap("Box/d_esquerda.png", PixmapFormat.RGB565 );
		Assets2D.imgD_Normal = g.newPixmap("Box/d_normal.png", PixmapFormat.RGB565 );
		
		Assets2D.imgLedOff = g.newPixmap("Box/led_off.png", PixmapFormat.RGB565 );
		Assets2D.imgLedOn = g.newPixmap("Box/led_on.png", PixmapFormat.RGB565 );
		
		Assets2D.imgMessage = g.newPixmap("Box/message.png", PixmapFormat.RGB565 );
		
		Assets2D.imgS_Dist = g.newPixmap("Box/s_dist.png", PixmapFormat.RGB565 );

		Assets2D.imgS_Toque = g.newPixmap("Box/s_toque.png", PixmapFormat.RGB565 );
		Assets2D.imgPos_S_Dist = g.newPixmap("Box/ps_dist.png", PixmapFormat.RGB565);
		Assets2D.imgTimer =  g.newPixmap("Box/timer.png", PixmapFormat.RGB565 );
	
		
		
		//For Loop
		Assets2D.imgLoopLeftUp = g.newPixmap("Box/Loop/leftUpLoop.png", PixmapFormat.RGB565 );
		Assets2D.imgLoopRightUp = g.newPixmap("Box/Loop/rightUpLoop.png", PixmapFormat.RGB565 );
		Assets2D.imgLoopMidle = g.newPixmap("Box/Loop/midleLoop.png", PixmapFormat.RGB565 );
		Assets2D.imgLoopLeftDown =  g.newPixmap("Box/Loop/leftDownLoop.png", PixmapFormat.RGB565 );
		Assets2D.imgLoopRightDown = g.newPixmap("Box/Loop/rightDownLoop.png", PixmapFormat.RGB565 );		
		
		
		StoreProgram.getStoreProgram().setLstProgram(new LinkedList<Box>());
		
        game.setScreen(new MainMenuRobot(game));
	}

	@Override
	public void present(float deltaTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
}
