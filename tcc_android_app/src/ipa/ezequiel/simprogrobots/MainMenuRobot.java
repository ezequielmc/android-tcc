package ipa.ezequiel.simprogrobots;

import java.util.List;

import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.program.StoreProgram;
import ipa.ezequiel.simprogrobots.Assets2D;

public class MainMenuRobot extends Screen {

	public MainMenuRobot(AndroidGame game){
		super(game);
	}

	@Override
    public void update(float deltaTime) {
        
        //Graphics g = game.getGraphics();
    	List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        
    	game.getInput().getKeyEvents();       
        
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(inBounds(event, 300, 10, 150, 50)) {
                	game.setProgramScreen();
                	return;
                }
                else if(inBounds(event, 300, 70, 150, 50) ) {
                	game.setSimScreen();
                	return;
                }
                if(inBounds(event, 300, 130, 150, 50) ) {
                	game.setSendScreen();
                	return;
                }
                
                if(inBounds(event, 300, 190 , 150, 50) ) {
                	game.setControlScreen();
                	return;
                }
                
                if(inBounds(event, 10, 210, 131, 90)){
                	game.setRobotScreen();
                	return;
                }
                
                if(inBounds(event, 150, 210, 131, 90)){
                	game.setScenarioScreen();
                	return;
                }
            }
        }
    }
    
    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
            return true;
        else
            return false;
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets2D.background, 0, 0);
        g.drawPixmap(Assets2D.logo , 40 , 15 );
        
        g.drawPixmap(Assets2D.txtProgramar, 300, 10 );
        g.drawPixmap(Assets2D.txtSimular, 300, 70 );
        g.drawPixmap(Assets2D.txtEnviar, 300, 130 );
        g.drawPixmap(Assets2D.txtControlar, 300, 190 );
     
        if( StoreProgram.getStoreProgram().getiRobot() == 1)
        	g.drawPixmap(Assets2D.imgRobot01Small, 15, 210);
        else
        	g.drawPixmap(Assets2D.imgRobot02Small, 15, 210);

        g.drawPixmap(Assets2D.imgBoxRobotSmall, 10, 210);
         
        
        if( StoreProgram.getStoreProgram().getiScenario() == 1){
        	g.drawPixmap(Assets2D.imgScenario01Small, 150, 210);
        }
        else if( StoreProgram.getStoreProgram().getiScenario() == 2){
        	g.drawPixmap(Assets2D.imgScenario02Small, 150, 210);
        }
        else{
        	g.drawPixmap(Assets2D.imgScenario03Small, 150, 210);
        }
        
    }

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
