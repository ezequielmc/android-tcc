package ipa.ezequiel.simprogrobots;

import java.io.File;

import android.os.Environment;
import ipa.ezequiel.simprogrobots.framework.Screen;


public class MainSimProgRobot extends AndroidGame {
	
	@Override
	public Screen getStartScreen() {
		
		/* Modificado para que o identificador seja apontado */
		/* TODO: Estudar melhor maneira, pois isto so e necessario */
		/* Pq precisamos carregar as imagens antes */
		LoadingScreen nLoad = new LoadingScreen(this);
		
		this.initScreens();
		return nLoad;
	}

	
}
