package ipa.ezequiel.simprogrobots;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.graphics.Color;
import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.Assets2D;
import ipa.ezequiel.simprogrobots.framework.program.Box;
import ipa.ezequiel.simprogrobots.framework.program.Menu;
import ipa.ezequiel.simprogrobots.framework.program.Option;
import ipa.ezequiel.simprogrobots.framework.program.StoreProgram;
import ipa.ezequiel.simprogrobots.framework.program.Tab;
import ipa.ezequiel.simprogrobots.framework.program.EnumBoxes;

public class ProgramScreen extends Screen {

	//---- Opcoes da aplicacao.
	private boolean isBoxSelected;
	private boolean isMovingWorkArea;
	private boolean isMovingTabs;
	
	
	private int indexTabSelected = 0;
	private Box selectedBox = null;
	
	private float posX;
	private float posY;
	
	//para acoes das caixas na area de trabalho
	private int posMoveX, posMoveY;
	
	//
	private int posMoveTabY;
	private Menu menu;
	
	public LinkedList<Box> lstProgram;
	public List<Box> lstInsertBox;
	
	private boolean bExecuted;
	public Box lastBox;
	
	//private Box spaceBox;
	private Box lastTouchedBox;
	private float sumTimeTouched;
	private boolean isTouchedDownBox;
	private boolean isTouchedUpBox;
	private boolean isGarbageSelected;
	private boolean progamHasAnIf;
	private float totalPressedTime;
	private boolean isBetweenTwoBoxes;
	private int posBetweenTwoBoxes;
	private int numberLoops = 0;
	
	public ProgramScreen(AndroidGame game) {
		super(game);
		bExecuted = false;
		
		isMovingTabs = false;
		isMovingWorkArea = false;
		posMoveTabY = 0;
	}

	void changeMenu(){
		Graphics g = game.getGraphics();
		
		menu = new Menu(g.getWidth(), g.getHeight() );
		//Cria a lista para Programacao
		lstProgram = StoreProgram.getStoreProgram().getLstProgram();
		lstProgram.clear();
		
		lstInsertBox = new ArrayList<Box>();
		
		Box startBox = new Box(-1, 5, 82, 20, "SPACE", Color.TRANSPARENT, Assets2D.imgStart );
		startBox.setsTypeComp(EnumBoxes.START);

		Box spaceBox = new Box(10, 40, 82, 86, "SPACE", Color.DKGRAY, Assets2D.imgEmptyBox );
		lstProgram.add(startBox);

		lstProgram.add(spaceBox);
		lstInsertBox.add(spaceBox);
		
		posMoveX = 0;
		setPosMoveY(g.getHeight() - 4);
		bExecuted = true;
		progamHasAnIf = false;
		
		
	}
	
	void initMenu() {
		if(!bExecuted){
			changeMenu();
		}
	}

	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
        
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();       
        
        //Para modificar as propriedades da caixa de selecao.
        if(isTouchedDownBox){
        	sumTimeTouched += deltaTime;
        	if(sumTimeTouched > 0.6f){
				isTouchedDownBox = false;
				isTouchedUpBox  = false;
        	}
        }

    	int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            
            //Eventos que acontecem enquanto se clica em algum objeto.
            if(event.type == TouchEvent.TOUCH_DOWN ){
            	
            	//Valida se havera uma troca de abas na aplicacao.
            	if( event.x > g.getWidth() - 20 ){
            		Tab tmpTab = null;
                    for( int index = 0 ; index < menu.getSizeLstTabs() ; index++ ){
                    	tmpTab = menu.getTab( index );
                    	if( inBounds( event , tmpTab.getiPosX() , tmpTab.getiPosY(), 20, tmpTab.getiMaxSize() ) ){
                    		menu.getTab(indexTabSelected).setSelected(false);
                    		indexTabSelected = index;
                    		posMoveTabY = 0;
                    		menu.getTab(indexTabSelected).setSelected(true);
                    	}
                    }
            	}
            	
            	//REGION Valida
            	else if( event.x > g.getWidth() - 100 ) {
            		Box tmpBox = null;
                    isMovingTabs = true;
            		for( int index = 0; index < menu.getTab(indexTabSelected).sizeLstBox() ; index++ ){
                    	tmpBox = menu.getTab(indexTabSelected).getBox(index);
                    	if( inBounds(event, tmpBox.getPosX() , tmpBox.getPosY() - posMoveTabY , tmpBox.getiSize(), tmpBox.getiSize()) ){
                 			isBoxSelected = true;
                 			isGarbageSelected = false;
                 			selectedBox = tmpBox;
                 			posX = event.x;
                 			posY = event.y;
                 			return;
                 		}
                    }
            	}
            	//ENDREGION Valida

            	
            	else{
            		//Valida se entrou em contato com alguma das opcoes do menu.
            		//TODO: Sempre que estiver objetos no Menu, tem que alterar.
            		if(!inBounds( event, 0, 0, 150, 32) ){
            			isMovingWorkArea = true;
            		}
            		
            		Box tmpBox = null;
            		//Valida se entrou em contato com uma das caixas de programacao
            		for( int index = 0; index < lstProgram.size() ; index++){
            			tmpBox = lstProgram.get(index);
            			if( inBounds(event, tmpBox.getPosX()- posMoveX, tmpBox.getPosY(), tmpBox.getiSize(), tmpBox.getiSize() ) ){
            				if( lstProgram.get(index).getsType() == "SPACE" )
								return;
					
            				//TODO: Move Objects
            				if( !isTouchedUpBox ){
            					totalPressedTime += deltaTime; 
            					if( totalPressedTime > 1.4f ){
            						isTouchedUpBox = false;
            						
            					}
            				}
            				
            				//TODO: Verifica se a caixa esta
            				if( !isTouchedDownBox ){
            					isTouchedDownBox = true;
            					lastTouchedBox = tmpBox;
            					sumTimeTouched = 0;
            				}
            				//TODO: PARA QUANDO FOR DELETAR OU ABRIR PROPRIEDADES
            				else if( lastTouchedBox == tmpBox && 
            						 sumTimeTouched < 0.6f &&
            						 isTouchedUpBox
            						 ){
            					
            						//Se o garbage esta ligado remove a caixa;
            						isTouchedDownBox  = false;
    								isTouchedUpBox  = false;
    								
    								//It`s not posible delete a blank space and endif statement
    								if( lstProgram.get(index).getsType().contains("SPACE") )
										return;
    								
									if( isGarbageSelected ) {
    									garbageItems(i, tmpBox, index);
									
									}else{
    									//TODO: Adicionar dados
    									lastBox = tmpBox;
            							game.setPropertiesScreen();
            						}
									
									
    								return;
            				}
                			
            				
            				if( lastTouchedBox != tmpBox ) {
            					isTouchedDownBox = false;
            					isTouchedUpBox  = false;
            				}
            			}
            		} 
            		posX = event.x;
            		isBoxSelected = false;
            		return;
            	}
            }
            
            //Verifica se ha movimentos
            else if(event.type == TouchEvent.TOUCH_DRAGGED){

            	if(isBoxSelected && event.x < (g.getWidth() - 100)){
            			posX = event.x;
            			posY = event.y;
            			isBetweenTwoBoxes = false;
            			for( int index = 0; index < lstProgram.size() - 2 ; index++){
                			Box tmpBox1 = lstProgram.get(index);
                			Box tmpBox2 = lstProgram.get(index+1);
                			//tmpBox.getPosX()- posMoveX
                			if( event.x > tmpBox1.getPosX() - posMoveX + 50 && 
                				event.x < tmpBox2.getPosX() - posMoveX + 50 &&
                				!( tmpBox1.getsType().compareTo("SPACE") != 0 || 
                				tmpBox2.getsType().compareTo("SPACE") != 0 ) &&
                				event.y >= tmpBox1.getPosY() &&
                				event.y <= tmpBox1.getPosY() + 72)	
                			{
                				isBetweenTwoBoxes = true;
                				posBetweenTwoBoxes = index;
                				return;
                			}
            			}
            	}
            	
            	else if(isMovingTabs){
            		int actualPos = (int) (posMoveTabY + (posY - event.y));
            		if( actualPos < 0)
            			posMoveTabY = 0;
            		else if( actualPos > (g.getHeight() - 75))
            			posMoveTabY = g.getHeight() - 75;
            		else
            			posMoveTabY -= ( event.y - posY);
            		posY = event.y;
            		
            	}

            	//Valida se esta se movendo a area de trabalho
            	if(isMovingWorkArea){
            		
            		int actualPos = (int) (posMoveX + (posX - event.x)) ;
            		
            		if( actualPos < 0)
            			posMoveX = 0;
            		else if( actualPos > ( ( lstProgram.size() * 85 ) - 150 ))
            			posMoveX = ( lstProgram.size() * 85 ) - 150;
            		else 
            			posMoveX -= ( event.x - posX );
            		
            		posX = event.x;
        
            	}
            	
            	//Valida se havera uma troca de abas na aplicacao.
            	else if( event.x > g.getWidth() - 20 ){
            		Tab tmpTab = null;
                    for( int index = 0 ; index < menu.getSizeLstTabs() ; index++ ){
                    	tmpTab = menu.getTab( index );
                    	if( inBounds( event , tmpTab.getiPosX() , tmpTab.getiPosY(), 20, tmpTab.getiMaxSize() ) ){
                    		menu.getTab(indexTabSelected).setSelected(false);
                    		indexTabSelected = index;
                    		menu.getTab(indexTabSelected).setSelected(true);
                    	}
                    }
            	}
            }
    
            //Verifica se e uma acao de quando se tira o dedo da tela.
            else if(event.type == TouchEvent.TOUCH_UP) {
            	
            	totalPressedTime = 0;
            	
            	//Funcao: Mostrar Propriedades de um box Or delete
            	if(isTouchedDownBox){
            		isTouchedUpBox = true;
            	}
            	
            	//Funcao: Para quando estiver movimentando a area de trabalho.
            	if( isMovingWorkArea ){
            		isMovingWorkArea = false;
            		return;
            	}
            	
            	//Funcao: Para quando estiver modificando a area de guias;
            	if( isMovingTabs ){
            		isMovingTabs = false;
            	}
            	
            	//Para que quando esteja na area de blocos nao adicione itens a linha de prog.
            	if( event.x > (g.getWidth() - 100)){
            		isBoxSelected = false;
            		return;
            	}
            	if( isBoxSelected && isBetweenTwoBoxes){
            		addNewBoxProgram(g, event);
            	}
            	//Verifica se o dedo esta sobre a ultima caixa da linha de programacao
            	if( isBoxSelected ){
	        		createNewBoxProgram(g, event);
	            }
	            
            	/*
            	 * Opcoes do Menu
            	 * 1 - Save
            	 * 2 - Load
            	 * 3 - Run
            	 */
            	if( inBounds( event, 0, 0, 32, 32) ){
            		game.showSaveDialog();
            	}
            	else if( inBounds( event, 40, 0, 32, 32) ){
            		game.showOpenDialog();
            	}
            	else if( inBounds( event, 70, 0, 32, 32) ){
            		game.setSimScreen();
            	}
            	else if( inBounds( event, 115, 0, 32, 32)){
            		isGarbageSelected = !isGarbageSelected;
            	}
            }
        }
	}

	private void garbageItems(int i, Box tmpBox, int index) {
		if(progamHasAnIf && tmpBox.getsTypeComp() != EnumBoxes.NOTHING){
			
			int lastPosX = 0;
			int count = 0;
			if( tmpBox.getsTypeComp() == EnumBoxes.IF_INIT ){
				lastPosX = tmpBox.getPosX(); 
				progamHasAnIf = false;
				
				//remove and change positions.
				for(int location = 1; location < lstProgram.size() ; location++){
					if(lstProgram.get(location).getsTypeComp() == EnumBoxes.NOTHING){
						if(lstProgram.get(location).getPosX() > lastPosX){
							lstProgram.get(location).setPosX(lastPosX + 92 * count); 
							count++;
						}
						continue;
					}
					lstProgram.remove(location);
					location--;
				}
				//remove from insert boxes list
				for(int location = 0; location < lstInsertBox.size(); location++){
					if(lstInsertBox.get(location).getsTypeComp() == EnumBoxes.NOTHING)
						continue;
					lstInsertBox.remove(location);
				}
				
			}
			else
			{
				for(int location = lstProgram.indexOf(tmpBox) ; location < lstProgram.size() ; location++){
					if( lstProgram.get(location).getsTypeComp() == tmpBox.getsTypeComp() )
						lstProgram.get(location).setPosX( lstProgram.get(location).getPosX() - 92 );
				}
				lstProgram.remove(tmpBox);
				
				int maxValue = 0;
				int indexEndIF = 0;
				for( int location = 0; location < lstProgram.size() ; location ++ ){
					if( lstProgram.get(location).getsTypeComp() == EnumBoxes.IF_FALSE ||
						lstProgram.get(location).getsTypeComp() == EnumBoxes.IF_TRUE )
						if(lstProgram.get(location).getPosX() > maxValue)
							maxValue = lstProgram.get(location).getPosX();
					
					if( lstProgram.get(location).getsTypeComp() == EnumBoxes.IF_END ){
						indexEndIF = location;
						break;
					}
				}
				
				for( int location = indexEndIF; location < lstProgram.size() ; location ++ ){
					if( lstProgram.get(location).getsTypeComp() != EnumBoxes.START)
						lstProgram.get(location).setPosX( maxValue + 92 + 92 * ( location - indexEndIF ) );	
				}												
			}
			
		}
		else{
			
			int contador = 0;
			int removed = 0;
			if( tmpBox.getiIndex() == 900){
				for( contador = 0; contador < lstProgram.size(); contador++ ){
					if( lstProgram.get(contador).getsType().contains(tmpBox.getsType()) ){
						
						if(lstProgram.get(contador).getsType().contains("SPACE_")){
							lstInsertBox.remove(lstProgram.get(contador));
						}
						lstProgram.remove(lstProgram.get(contador));
						removed += 1;
						contador -= 1;
					}
				}
			}
			else{
				lstProgram.remove(tmpBox);
				removed = 1;
			}
			
			for( int nBox = index; nBox < lstProgram.size(); nBox++ ){
				lstProgram.get(nBox).setPosX( lstProgram.get(nBox).getPosX() - 94 * removed);
			}
		}
	}

	
	
	//TODO: add and create box, study on both the changes
	private void addNewBoxProgram(Graphics g, TouchEvent event) {
		
		if( selectedBox.getsType() == "IF" && progamHasAnIf )
			return;
		
		if( !progamHasAnIf )
		progamHasAnIf = selectedBox.getsType() == "IF" ? true : false;
		
		
		boolean isIfSelected = selectedBox.getsType() == "IF" ? true : false;
		
		Box tmpBox = lstProgram.get(posBetweenTwoBoxes + 1);
		Box copy = new Box( tmpBox.getiIndex(), tmpBox.getPosX() + 5  , tmpBox.getPosY(), 
							   selectedBox.getiSize(),
					           selectedBox.getsType(), 
					           selectedBox.getiColor(), 
					           selectedBox.getImage() ) ;
		copy.setsTypeComp(tmpBox.getsTypeComp());

		
		Option tmpOption = null;
		for(int pos = 0; pos < selectedBox.getOptions().size() ; pos++){
			tmpOption = selectedBox.getOptions().get(pos);
			try {
				copy.addOption( tmpOption.clone() );
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if( isIfSelected ){
			
			Box tmp1 = lstProgram.get(lstProgram.size() - 1).clone();
			
			tmp1.setsTypeComp(EnumBoxes.IF_TRUE);
			tmp1.setPosX( tmpBox.getPosX() + 90 );
			tmp1.setPosY( tmpBox.getPosY() - 50 );
			
			Box tmp2 = lstProgram.get(lstProgram.size() - 1).clone();
			tmp2.setsTypeComp(EnumBoxes.IF_FALSE);
			tmp2.setPosX( tmpBox.getPosX() + 90 );
			tmp2.setPosY( tmpBox.getPosY() + 50 );
			
			Box endIF = new Box(3,tmp1.getPosX() + 90 , tmp1.getPosY() + 50 , 72, "SPACE", Color.TRANSPARENT, Assets2D.imgEndIf);
			endIF.setsTypeComp(EnumBoxes.IF_END);

			copy.setsTypeComp(EnumBoxes.IF_INIT);
			copy.setsType("IF");
			

			for( int i = posBetweenTwoBoxes + 1; i < lstProgram.size(); i++ ){
				lstProgram.get(i).setPosX( lstProgram.get(i).getPosX() + 92 * 3 );
			}

			lstProgram.add( posBetweenTwoBoxes + 1 , copy);
			
			lstProgram.add(posBetweenTwoBoxes + 2,tmp1);
			lstInsertBox.add(tmp1);
			
			lstProgram.add(posBetweenTwoBoxes + 3, tmp2);
			lstInsertBox.add(tmp2);
			
			lstProgram.add(posBetweenTwoBoxes + 4, endIF);
			
		}
		else {
			int hasLoop = 0;
			
			if( copy.getsType().contains("D_DIREITA") || 
				copy.getsType().contains("D_ESQUERDA") || 
				copy.getsType().contains("D_NORMAL") || 
				copy.getsType().contains("S_DIST") || 
				copy.getsType().contains("TOQUE") )
				hasLoop = 1;
			
			for( int i = posBetweenTwoBoxes + 1; i < lstProgram.size(); i++ ){
				lstProgram.get(i).setPosX( lstProgram.get(i).getPosX() + 96 + hasLoop * 94 );
			}
			
			lstProgram.add(  posBetweenTwoBoxes + 1  , copy);
			
			if( hasLoop == 1 ){
				copy.setsType( copy.getsType() + numberLoops );
				
				numberLoops++;
				Box tmp1 = lstProgram.get(lstProgram.size() - 1).clone();
				tmp1.setsTypeComp(EnumBoxes.NOTHING);
				tmp1.setsType("SPACE_" + copy.getsType() + numberLoops );
				tmp1.setPosX( copy.getPosX() + 94 );
				tmp1.setPosY( copy.getPosY()  );
				
				lstProgram.add(posBetweenTwoBoxes + 2, tmp1);
				lstInsertBox.add(tmp1);
				
			}
		
			
		}
		//TODO: Mostra tela de Propriedades.
		lastBox = copy;
		game.setPropertiesScreen();
		
		isBoxSelected = false;
		isBetweenTwoBoxes = false;
		posBetweenTwoBoxes = -1;
				
	}

	private void createNewBoxProgram(Graphics g, TouchEvent event) {

		int indexBoxOnProgram;
		//Coloca a selecao de um box para descelecionado
		isBoxSelected = false;

		for( int index = 0; index < lstInsertBox.size() ; index++){
			Box tmpBox = lstInsertBox.get(index);
			
			if( inBounds( event, tmpBox.getPosX() - posMoveX, tmpBox.getPosY(), tmpBox.getiSize(), tmpBox.getiSize())){
				
				if( selectedBox.getsType() == "IF" && progamHasAnIf )
					return;
				
				if( !progamHasAnIf )
				progamHasAnIf = selectedBox.getsType() == "IF" ? true : false;
				
				
				boolean isIfSelected = selectedBox.getsType() == "IF" ? true : false;
				
				
				Box copy = new Box( selectedBox.getiIndex() , tmpBox.getPosX() , tmpBox.getPosY(), 
									   selectedBox.getiSize(),
							           selectedBox.getsType() , 
							           selectedBox.getiColor(), 
							           selectedBox.getImage() ) ;
				copy.setsTypeComp(tmpBox.getsTypeComp());

				
				copy.clearOptions();
				Option tmpOption = null;
				for(int pos = 0; pos < selectedBox.getOptions().size() ; pos++){
					tmpOption = selectedBox.getOptions().get(pos);
					
					try {
						copy.addOption( tmpOption.clone() );
					} catch (CloneNotSupportedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if( isIfSelected ){
					
					Box tmp1 = lstProgram.get(lstProgram.size() - 1).clone();
					tmp1.setsTypeComp(EnumBoxes.IF_TRUE);
					tmp1.setPosX( tmp1.getPosX() + 92 );
					tmp1.setPosY( tmp1.getPosY() - 50 );
					
					Box tmp2 = lstProgram.get(lstProgram.size() - 1).clone();
					tmp2.setsTypeComp(EnumBoxes.IF_FALSE);
					tmp2.setPosX( tmp2.getPosX() + 92 );
					tmp2.setPosY( tmp2.getPosY() + 50 );
					
					Box endIF = new Box(3,tmp1.getPosX() + 92 , tmp1.getPosY() + 50 , 72, "SPACE", Color.TRANSPARENT, Assets2D.imgEndIf);
					endIF.setsTypeComp(EnumBoxes.IF_END);

					Box tmp3 = lstProgram.get(lstProgram.size() - 1).clone();
					tmp3.setsTypeComp(EnumBoxes.NOTHING);
					tmp3.setPosX( tmp3.getPosX()  + 92 * 3 );
					
					copy.setsTypeComp(EnumBoxes.IF_INIT);
					copy.setsType("IF");
					
					lstProgram.remove(lstProgram.size() - 1);
					lstInsertBox.remove(lstInsertBox.size() - 1);
					
					lstProgram.add(  lstProgram.size()  , copy);
					
					
					lstProgram.add(tmp1);
					lstInsertBox.add(tmp1);
					
					lstProgram.add(tmp2);
					lstInsertBox.add(tmp2);
					
					lstProgram.add(endIF);
					
					lstProgram.add(tmp3);
					lstInsertBox.add(tmp3);
					
					
				}
				else {
					if(progamHasAnIf){
						switch (tmpBox.getsTypeComp()) {
						case NOTHING:
							
							int hasLoop = 0;
							int inLoop = 0;
							
							
							//Verifica se a nova caixa e um loop
							if( copy.getsType().contains("D_DIREITA") || 
								copy.getsType().contains("D_ESQUERDA") || 
								copy.getsType().contains("D_NORMAL") || 
								copy.getsType().contains("S_DIST") || 
								copy.getsType().contains("TOQUE") )
								hasLoop = 1;
							
							//Verificar se a nova caixa esta dentro de um 
							if( tmpBox.getsType().contains("D_DIREITA") || 
								tmpBox.getsType().contains("D_ESQUERDA") || 
								tmpBox.getsType().contains("D_NORMAL") || 
								tmpBox.getsType().contains("S_DIST") || 
								tmpBox.getsType().contains("TOQUE") ) 
								inLoop = 1;
							
							posBetweenTwoBoxes = lstProgram.indexOf(tmpBox);
							
							if( !(hasLoop == 1 && inLoop == 1) ){
								for( int i = posBetweenTwoBoxes; i < lstProgram.size(); i++ ){
									lstProgram.get(i).setPosX( lstProgram.get(i).getPosX() + 96 + hasLoop * 96);
								}
							}
							//Se nao esta dentro de um loop e nao e um loop, somente adiciona
							if( hasLoop == 0 && inLoop == 0)
								lstProgram.add(  posBetweenTwoBoxes  , copy);
							
							//Se e um loop e nao esta dentro de um loop, adiciona as duas novas caixas
							else if( hasLoop == 1 && inLoop == 0 ){
								lstProgram.add(  posBetweenTwoBoxes , copy);
								copy.setsType( copy.getsType() + numberLoops );
								
								numberLoops++;
								Box tmp1 = lstProgram.get(lstProgram.size() - 1).clone();
								tmp1.setsTypeComp(EnumBoxes.NOTHING);
								tmp1.setsType("SPACE_" + copy.getsType() + numberLoops );
								tmp1.setPosX( copy.getPosX() + 94 );
								tmp1.setPosY( copy.getPosY()  );
								
								lstProgram.add(posBetweenTwoBoxes + 1, tmp1);
								lstInsertBox.add(tmp1);
							}
							else if( inLoop == 1 && hasLoop == 0 ){
								lstProgram.add(  posBetweenTwoBoxes , copy);
								copy.setsType( copy.getsType() + tmpBox.getsType().replace("BLANK_", ""));
							}
							else{
								isBoxSelected = false;
								isMovingTabs = false;
								isMovingWorkArea = false;
								return;
							}
							
							break;
						case IF_TRUE:
						case IF_FALSE:
							//indexBoxOnProgram shows the position to realize the insertion
							
							if( copy.getsType().contains("D_DIREITA") || 
									copy.getsType().contains("D_ESQUERDA") || 
									copy.getsType().contains("D_NORMAL") || 
									copy.getsType().contains("S_DIST") || 
									copy.getsType().contains("TOQUE") )
								return;
							
							indexBoxOnProgram = lstProgram.indexOf(tmpBox);
							lstProgram.add(indexBoxOnProgram  , copy);
							
							lstProgram.get(  indexBoxOnProgram + 1 ).setPosX( tmpBox.getPosX() + 92 );
							
							//Encontra o maximo para poder realizar a movimentacao das caixas posteriores
							int maxValue = 0;
							int indexEndIF = 0;
							for( int i = 0; i < lstProgram.size() ; i ++ ){
								if( lstProgram.get(i).getsTypeComp() == EnumBoxes.IF_FALSE ||
									lstProgram.get(i).getsTypeComp() == EnumBoxes.IF_TRUE )
									if(lstProgram.get(i).getPosX() > maxValue)
										maxValue = lstProgram.get(i).getPosX();
								
								if( lstProgram.get(i).getsTypeComp() == EnumBoxes.IF_END ){
									indexEndIF = i;
									break;
								}
							}
							
							for( int i = indexEndIF; i < lstProgram.size() ; i ++ ){
									lstProgram.get(i).setPosX( maxValue + 92 + 92 * ( i - indexEndIF ) );	
							}
							break;
							
						default:
							break;
						}
					}
					else {
						
						int hasLoop = 0;
						int inLoop = 0;
						
						
						//Verifica se a nova caixa e um loop
						if( copy.getsType().contains("D_DIREITA") || 
							copy.getsType().contains("D_ESQUERDA") || 
							copy.getsType().contains("D_NORMAL") || 
							copy.getsType().contains("S_DIST") || 
							copy.getsType().contains("TOQUE") )
							hasLoop = 1;
						
						//Verificar se a nova caixa esta dentro de um 
						if( tmpBox.getsType().contains("D_DIREITA") || 
							tmpBox.getsType().contains("D_ESQUERDA") || 
							tmpBox.getsType().contains("D_NORMAL") || 
							tmpBox.getsType().contains("S_DIST") || 
							tmpBox.getsType().contains("TOQUE") ) 
							inLoop = 1;
						
						posBetweenTwoBoxes = lstProgram.indexOf(tmpBox);
						
						if( !(hasLoop == 1 && inLoop == 1) ){
							for( int i = posBetweenTwoBoxes; i < lstProgram.size(); i++ ){
								lstProgram.get(i).setPosX( lstProgram.get(i).getPosX() + 96 + hasLoop * 96);
							}
						}
						
						//Se nao esta dentro de um loop e nao e um loop, somente adiciona
						if( hasLoop == 0 && inLoop == 0)
							lstProgram.add(  posBetweenTwoBoxes  , copy);
						
						//Se e um loop e nao esta dentro de um loop, adiciona as duas novas caixas
						else if( hasLoop == 1 && inLoop == 0 ){
							lstProgram.add(  posBetweenTwoBoxes , copy);
							numberLoops++;
							copy.setsType( copy.getsType() + numberLoops );
							
							Box tmp1 = lstProgram.get(lstProgram.size() - 1).clone();
							tmp1.setsTypeComp(EnumBoxes.NOTHING);
							tmp1.setsType("SPACE_" + copy.getsType() );
							tmp1.setPosX( copy.getPosX() + 94 );
							tmp1.setPosY( copy.getPosY()  );
							
							lstProgram.add(posBetweenTwoBoxes + 1, tmp1);
							lstInsertBox.add(tmp1);
						}
						else if( inLoop == 1 && hasLoop == 0 ){
							lstProgram.add(  posBetweenTwoBoxes , copy);
							copy.setsType( copy.getsType() + tmpBox.getsType().replace("SPACE_", ""));
						}
						else{
							isBoxSelected = false;
							isMovingTabs = false;
							isMovingWorkArea = false;
							return;
						}
						

					}
					
					
				}
				
				//TODO: Mostra tela de Propriedades.
				lastBox = copy;
				game.setPropertiesScreen();
			}
		}
	}
            
    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
    	if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
    			return true;
        else
                return false;
    }
    
	@Override
	public void present(float deltaTime) {
		
		Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets2D.background, 0, 0);
		drawProgram(g);
        drawTabsAndOthers(g);
        
        //Desenha a caixa se movimentando no cenario
        if(isBoxSelected && posX < (g.getWidth() - 120 ) ){
        	if( selectedBox.getImage() == null )
        		g.drawRect( (int)posX - 35 , (int)posY - 35, 70 , 70 , selectedBox.getiColor() ); 
        	else
        		g.drawPixmap( selectedBox.getImage(), (int)posX - 36 , (int)posY - 36);
        }
        
        drawMenu(g);

	}

	private void drawMenu(Graphics g) {
		// Desenha o Menu do programa
        g.drawPixmap(Assets2D.imgSave, 4, 0 );
        g.drawPixmap(Assets2D.imgLoad, 40, 0 );
        g.drawPixmap(Assets2D.imgRun, 72,0 );
        
        if(isGarbageSelected)
        	g.drawPixmap(Assets2D.imgGarbageSelected, 115,0 );
        else
        	g.drawPixmap(Assets2D.imgGarbage, 115,0 );
        
	}

	private void drawTabsAndOthers(Graphics g) {
		Box tmpBox;
		
		//Desenha as abas
        g.drawLine( g.getWidth() - 100 , 0, g.getWidth() - 100 , g.getHeight() , Color.LTGRAY );
        g.drawRect( g.getWidth() - 100 , 0 , 130 , g.getHeight() + 1  ,  menu.getTab(indexTabSelected).getiColor() );
        
        Tab tmpTab = null;
        for( int i = 0 ; i < menu.getSizeLstTabs() ; i++ ){
        	tmpTab = menu.getTab( i );
        	// x, y, largura, altura .
        	g.drawRect(tmpTab.getiPosX() , tmpTab.getiPosY()
        								 , tmpTab.getiMaxSize() 
        								 , (g.getHeight() / menu.getSizeLstTabs() )
        							     , tmpTab.getiColor() );
        }
        
        tmpBox = null;
        for( int i = 0; i < menu.getTab(indexTabSelected).sizeLstBox() ; i++ ){
        	tmpBox = menu.getTab(indexTabSelected).getBox(i);
        	g.drawPixmap(tmpBox.getImage(), tmpBox.getPosX() , tmpBox.getPosY() - posMoveTabY);
        	//g.drawRect(tmpBox.getPosX() , tmpBox.getPosY(), tmpBox.getiSize(), tmpBox.getiSize(), tmpBox.getiColor());
        }
        
        // Desenha a Barra de Rolagem no canto inferior direito.
        /*
        TODO: Se possivel, arrumar a barra de rolagem inferior.
        int maxTam = (g.getWidth() -150);
        int actualProgram = ( lstProgram.size() * 85 );
        int factorTam = maxTam / actualProgram;
        if( factorTam > maxTam )
        	factorTam = maxTam;
        else if( factorTam < 5 ){
        	factorTam = 5;
        }
        g.drawRect( (posMoveX/actualProgram) * maxTam - 5, posMoveY, factorTam ,6, Color.WHITE);
        */
	}

	private void drawProgram(Graphics g) {
		//Desenha as caixas de programacao.
        Box tmpBox = null;
        for( int i = 0; i < lstProgram.size() ; i++ ){
        	tmpBox = lstProgram.get(i);
        	
        	if( tmpBox.getsType() != "SPACE" && tmpBox.getiIndex() != -1 )
        		g.drawPixmap(Assets2D.imgBorder, tmpBox.getPosX() - posMoveX , tmpBox.getPosY() );
        	
        	if(tmpBox.getImage() == null) 
           		g.drawRect( tmpBox.getPosX()- posMoveX , tmpBox.getPosY(), tmpBox.getiSize(), tmpBox.getiSize(), tmpBox.getiColor());
        	 else
            	g.drawPixmap(tmpBox.getImage(), tmpBox.getPosX()- posMoveX , tmpBox.getPosY());
        
        	if( tmpBox.getiIndex() == 900){
        		g.drawPixmap(Assets2D.imgLoopLeftUp, tmpBox.getPosX()- posMoveX , tmpBox.getPosY() - 40 );
        		g.drawPixmap(Assets2D.imgLoopLeftDown, tmpBox.getPosX()- posMoveX , tmpBox.getPosY() + 86);
        	}
        	else if((tmpBox.getsType().contains("D_DIREITA") || 
        			tmpBox.getsType().contains("D_ESQUERDA") || 
        			tmpBox.getsType().contains("D_NORMAL") || 
        			tmpBox.getsType().contains("S_DIST") || 
        			tmpBox.getsType().contains("TOQUE") ) && !tmpBox.getsType().contains("SPACE")) {
        		g.drawPixmap(Assets2D.imgLoopMidle, tmpBox.getPosX()- posMoveX -8 , tmpBox.getPosY() - 41 );
        		g.drawPixmap(Assets2D.imgLoopMidle, tmpBox.getPosX()- posMoveX -8 , tmpBox.getPosY() + 98);
        	
        	}
        	else if( tmpBox.getsType().contains("SPACE_") ){
        		g.drawPixmap(Assets2D.imgLoopRightUp, tmpBox.getPosX()- posMoveX - 10, tmpBox.getPosY() - 40 );
        		g.drawPixmap(Assets2D.imgLoopRightDown, tmpBox.getPosX()- posMoveX - 10, tmpBox.getPosY() + 86);
        		
        	}
        	 
        	    
        		
        		
        	int posTotalY = tmpBox.getPosY() + 100;
        	for( String a : tmpBox.getLstValueOptions()){
        		g.drawText( a, tmpBox.getPosX() + 46 - posMoveX , posTotalY, 18, Color.BLACK);
        		posTotalY += 28;
        	}
        	
        	
        }
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		Graphics g = game.getGraphics();
		g.drawRect(10, 10, 100, 100, Color.YELLOW );
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	public int getPosMoveY() {
		return posMoveY;
	}

	public void setPosMoveY(int posMoveY) {
		this.posMoveY = posMoveY;
	}

}
