package ipa.ezequiel.simprogrobots;

import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.program.Box;
import ipa.ezequiel.simprogrobots.framework.program.CheckOption;
import ipa.ezequiel.simprogrobots.framework.program.DegreeOption;
import ipa.ezequiel.simprogrobots.framework.program.Option;
import ipa.ezequiel.simprogrobots.framework.program.StepsOption;
import ipa.ezequiel.simprogrobots.framework.program.TimeOption;
import ipa.ezequiel.simprogrobots.framework.program.VelocityOption;

import java.util.List;

import android.graphics.Color;

public class PropertiesScreen extends Screen {

	private boolean isOptionSelected;
	private int lastOptionY;
	private Option selectedOption;

	public PropertiesScreen(AndroidGame game) {
		super(game);
		isOptionSelected = false;
	}
	
	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
		Box temp = game.pScreen.lastBox;
		
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();       
        
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            
            
            if(event.type == TouchEvent.TOUCH_DOWN){
            	updateOptions("B", deltaTime, event, temp.getOptions());
            }
            else if(event.type == TouchEvent.TOUCH_DRAGGED){
            	updateOptions("D", deltaTime, event, temp.getOptions());
            }
            if(event.type == TouchEvent.TOUCH_UP) {
            
            	
            	
            	updateOptions("U", deltaTime,event,temp.getOptions());
            	isOptionSelected = false;
            	
            	//Verifica se clicou sobre o OK.
            	if(inBounds(event, g.getWidth() - 130, g.getHeight() - 70, 100, 40) && !isOptionSelected){
            		makeStringOptions(temp);
            		game.onBackPressed();
            	}
            	
            	
            }
            
        }

	}
	
	
	private void makeStringOptions(Box temp) {
		
		temp.getLstValueOptions().clear();
		
		for( int index = 0; index < temp.getOptions().size() ; index++){
			
			switch ( temp.getOptions().get(index).getTipo() ) {
			//present time Option
			case "Time":
				TimeOption tOption = (TimeOption) temp.getOptions().get(index);
				temp.getLstValueOptions().add( tOption.getActualValue() + "s");
				break;

			case "Degree":
				DegreeOption dOption = (DegreeOption) temp.getOptions().get(index);
				temp.getLstValueOptions().add( ((int)dOption.getActualValue()) + "º");
				break;
			
			case "Velocity":
				VelocityOption vOption = (VelocityOption) temp.getOptions().get(index);
				temp.getLstValueOptions().add( vOption.getActualValue() + "cm/s");
				break;
			
			case "Steps":
				StepsOption stepOption = (StepsOption) temp.getOptions().get(index);
				temp.getLstValueOptions().add( ((int)stepOption.getActualValue()) + "cm");
				break;
            }	
		}
		
	}



	private void updateOptions(String type, float deltaTime, TouchEvent event, List<Option> option){
		
		if( !isOptionSelected ){
			//Digo que foi selecionada uma caixa
			lastOptionY = 60;
			for( int index = 0; index < option.size() ; index++){
				switch ( option.get(index).getTipo() ) {
				//present time Option
				case "Check":
					CheckOption opt = (CheckOption) option.get(index);
					for( int i = 0; i < opt.size ; i++){
						
						if(inBounds(event, 30, lastOptionY, 150, 32) && type == "U" ){
							opt.setCheckedItem(i);
						}
						
						lastOptionY += 45;
					}
					break;
				case "Time":
					if(inBounds(event, 95, lastOptionY - 15 , 335, 20) ){
						selectedOption = option.get(index);
						isOptionSelected = true;
						return;
					}
					lastOptionY += 70;
					break;
				case "Degree":
					if(inBounds(event, 95, lastOptionY - 15 , 335, 20) ){
						selectedOption = option.get(index);
						isOptionSelected = true;
						return;
					}
					lastOptionY += 70;
					break;
				case "Velocity":
					if(inBounds(event, 125, lastOptionY - 15 , 300, 19) ){
						selectedOption = option.get(index);
						isOptionSelected = true;
						return;
					}
					lastOptionY += 70;
					break;
				case "Steps":
					if(inBounds(event, 125, lastOptionY - 15 , 300, 19) ){
						selectedOption = option.get(index);
						isOptionSelected = true;
						return;
					}
					lastOptionY += 70;
					break;
	            }
			}
		}
		else {
			switch( selectedOption.getTipo() ){
				case "Time":
					updateTimeOption(event, selectedOption);
					break;
				case "Velocity":
					updateVelocityOption(event, selectedOption);
					break;	
				case "Steps":
					updateStepsOption(event, selectedOption);
					break;
				case "Degree":
					updateDegreeOption(event, selectedOption);
					break;
				
			}
		}	
	}
	
	
	//********* Apresenta Opcoes e valores ***********/
	private void updateStepsOption(TouchEvent event, Option option) {
		StepsOption sOption = (StepsOption) option;
		int actualPos = event.x - 130;
		int maxPx = 330;
		
		if(actualPos < 0)
			actualPos = 0;
		else if (actualPos > maxPx)
			actualPos = maxPx;
		
		float steps = actualPos / (maxPx / ( sOption.getMaxValue() - sOption.getMinValue())) + 
				sOption.getMinValue();
		
		steps = (float) (Math.round( steps ) );
		sOption.setActualValue( steps );
		
	}
	private void updateDegreeOption(TouchEvent event, Option option) {
		DegreeOption dOption = (DegreeOption) option;
		int actualPos = event.x - 130;
		int maxPx = 330;
		
		if(actualPos < 0)
			actualPos = 0;
		else if (actualPos > maxPx)
			actualPos = maxPx;
		
		float steps = actualPos / (maxPx / ( dOption.getMaxValue() - dOption.getMinValue())) + 
				dOption.getMinValue();
		steps = (float) (Math.round( steps ));
		dOption.setActualValue( steps );
		
	}
	private void updateVelocityOption(TouchEvent event, Option option) {
		VelocityOption vOption = (VelocityOption) option;
		int actualPos = event.x - 130;
		int maxPx = 330;
		
		if(actualPos < 0)
			actualPos = 0;
		else if (actualPos > maxPx)
			actualPos = maxPx;
		
		float velocity = actualPos / (maxPx / ( vOption.getMaxValue() - vOption.getMinValue())) + 
				vOption.getMinValue();
		velocity = (float) (Math.round( velocity * 100.00 ) / 100.00);
		vOption.setActualValue( velocity );
	}
	private void updateTimeOption(TouchEvent event, Option option) {
		TimeOption tOption = (TimeOption) option;
		int actualPos = event.x - 100;
		int maxPx = 330;
		
		if(actualPos < 0)
			actualPos = 0;
		else if (actualPos > maxPx)
			actualPos = maxPx;
		
		float time = actualPos / (maxPx / ( tOption.getMaxValue() - tOption.getMinValue())) + 
				tOption.getMinValue();
		time = (float) (Math.round( time));
		tOption.setActualValue( time );
	}
	
	
	@Override
	public void present(float deltaTime) {
		
		Graphics g = game.getGraphics();
		Box temp = game.pScreen.lastBox;
		
		if(temp.getOptions().size() == 0){
    		game.onBackPressed();
    		return;
		}
		
		g.drawPixmap(Assets2D.backgroundProperties, 0, 0);
		
		//Mostrar Options.
		this.presentOptions(deltaTime, g, temp.getOptions() );
		
		//Button of OK;
		//g.drawRect(x, y, width, height, color);
		g.drawRect(g.getWidth() - 130, g.getHeight() - 70, 100, 40, Color.parseColor("#444444"));
		g.drawText("OK", g.getWidth() - 105, g.getHeight() - 40 , 30, Color.parseColor("#EAEAEA"));
	
	}
	
	
	//*** Mostra as opcoes **//
	private void presentOptions(float deltaTime, Graphics g, List<Option> option){

		int lastDrawY = 60;
		int actualPos = 0;
		int maxValue;
		for( int index = 0; index < option.size() ; index++){
			
			switch ( option.get(index).getTipo() ) {
			//present time Option
			case "Check":
				CheckOption opt = (CheckOption) option.get(index);
				for( int i = 0; i < opt.size ; i++){
					
					if( opt.checkedItem(i))
						g.drawPixmap(Assets2D.imgChecked, 30, lastDrawY);
					else
						g.drawPixmap(Assets2D.imgUnchecked, 30, lastDrawY);
					
					g.drawText(opt.valueItem(i), 65, lastDrawY + 27 , 25, Color.BLACK);
					lastDrawY += 45;
				}
				
				break;
			case "Time":
				TimeOption tOption = (TimeOption) option.get(index);
				int maxPx = 330;
				g.drawText("Tempo:", 30, lastDrawY, 20, Color.GRAY);
				
				//Mostra a aba
				g.drawRect(100, lastDrawY - 7     , maxPx, 5, Color.GRAY);
				
				actualPos = (int) ((tOption.getActualValue() - tOption.getMinValue()) * 
						(maxPx / (tOption.getMaxValue() - tOption.getMinValue()) ));
				g.drawRect(100 + actualPos, lastDrawY - 15    , 7, 19, Color.DKGRAY);
				
				g.drawText( Float.toString(tOption.getActualValue())+"s", (g.getWidth()/2) + 30 , lastDrawY +30 , 20, Color.GRAY);
				lastDrawY += 70;
				break;

			case "Degree":
				DegreeOption dOption = (DegreeOption) option.get(index);
				maxValue = 330;
				g.drawText("Angulo:", 30, lastDrawY, 20, Color.GRAY);
				
				//Mostra a aba
				g.drawRect(100, lastDrawY - 7 , maxValue, 5, Color.GRAY);
				
				actualPos = (int) ((dOption.getActualValue() - dOption.getMinValue()) * 
						(maxValue / (dOption.getMaxValue() - dOption.getMinValue()) ));
				g.drawRect(100 + actualPos, lastDrawY - 15    , 7, 19, Color.DKGRAY);
				
				g.drawText( Float.toString(dOption.getActualValue())+"º", (g.getWidth()/2) + 30 , lastDrawY +30 , 20, Color.GRAY);
				lastDrawY += 70;
				break;
			
			case "Velocity":
				VelocityOption vOption = (VelocityOption) option.get(index);
				int maxvPx = 300;
				g.drawText("Velocidade:", 30, lastDrawY, 20, Color.GRAY);
				g.drawRect(130, lastDrawY - 7     , maxvPx, 5, Color.GRAY);
				
				actualPos = (int)((vOption.getActualValue() - vOption.getMinValue()) * 
						(maxvPx / (vOption.getMaxValue() - vOption.getMinValue())));
				
				g.drawRect(130 + actualPos , lastDrawY - 15    , 7, 19, Color.DKGRAY);
				
				g.drawText( Float.toString(vOption.getActualValue())+"cm/s", (g.getWidth()/2) + 30 , 
						lastDrawY +30 , 20, Color.GRAY);
				lastDrawY += 70;
				break;
			
			case "Steps":
				StepsOption stepOption = (StepsOption) option.get(index);
				int maxsPx = 300;
				g.drawText("Distancia:", 30, lastDrawY, 20, Color.GRAY);
				g.drawRect(130, lastDrawY - 7     , maxsPx, 5, Color.GRAY);
				
				actualPos = (int)((stepOption.getActualValue() - stepOption.getMinValue()) * 
						(maxsPx / (stepOption.getMaxValue() - stepOption.getMinValue())));
				
				g.drawRect(130 + actualPos , lastDrawY - 15    , 7, 19, Color.DKGRAY);
				
				g.drawText( Float.toString(stepOption.getActualValue())+"cm", (g.getWidth()/2) + 30 , 
						lastDrawY +30 , 20, Color.GRAY);
				lastDrawY += 70;
				break;
            }
			
		}

	}

	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
    	if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
    			return true;
        else
                return false;
    }
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
