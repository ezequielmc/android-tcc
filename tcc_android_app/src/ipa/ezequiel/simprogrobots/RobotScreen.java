package ipa.ezequiel.simprogrobots;

import java.util.List;

import android.R.color;
import android.graphics.Color;
import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.framework.program.StoreProgram;
import ipa.ezequiel.simprogrobots.Assets2D;

public class RobotScreen extends Screen {

	public RobotScreen(AndroidGame game) {
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
        
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        
    	game.getInput().getKeyEvents();       
        
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if( inBounds( event, 5, 40, 210, 158)){
                	StoreProgram.getStoreProgram().setiRobot(1);
                }
                if( inBounds( event, 240, 40, 210, 158)){
                	StoreProgram.getStoreProgram().setiRobot(2);
                }
                if( inBounds(event, 5, 240, g.getWidth() - 10, 40)){
                	game.pScreen.changeMenu();
                	game.onBackPressed();
                }
            }
        }

	}

	@Override
	public void present(float deltaTime) {
		Graphics g = game.getGraphics();
		
		g.drawPixmap(Assets2D.background, 0, 0);

		g.drawPixmap(Assets2D.imgRobot01,16,60);
		g.drawPixmap(Assets2D.imgRobot02,250,60);
		
		if( StoreProgram.getStoreProgram().getiRobot() == 1 ){
			g.drawPixmap(Assets2D.imgSelectedBoxRobot, 5, 60);
			g.drawPixmap(Assets2D.imgBoxRobot, 240, 60);
		}
		else{
			g.drawPixmap(Assets2D.imgBoxRobot, 5, 60);
			g.drawPixmap(Assets2D.imgSelectedBoxRobot, 240, 60);
		}

		g.drawText("Selecione um robo", 10, 40, 30, Color.BLACK);
		g.drawRect(5, 240, g.getWidth() - 10, 40, Color.GRAY);
		g.drawText("OK", 225, 270, 30, Color.WHITE);
		
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
    	if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
    			return true;
        else
                return false;
    }
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
