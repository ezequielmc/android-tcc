package ipa.ezequiel.simprogrobots;

import java.util.List;

import android.graphics.Color;
import android.text.method.Touch;
import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.framework.program.StoreProgram;
import ipa.ezequiel.simprogrobots.Assets2D;

public class ScenarioScreen extends Screen {

	private int touchedX;
	private boolean isMoving;
	private int posX = - 30;
	private int MAX_POSX = 250;
	private int MIN_POSX = -30;
	
	public ScenarioScreen(AndroidGame game) {
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
        
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        
    	game.getInput().getKeyEvents();       
        
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_DOWN){
            	touchedX = event.x;
            }
            else if(event.type == TouchEvent.TOUCH_DRAGGED){
            	posX -= (event.x - touchedX);
            	touchedX = event.x;
            	isMoving = true;
            }
            else if(event.type == TouchEvent.TOUCH_UP) {
            	if(!isMoving){
            		if(inBounds(event, 10 - posX, 60, 220, 130)){
            			StoreProgram.getStoreProgram().setiScenario(1);
            		}
            		if(inBounds(event, 240 - posX, 60, 220, 130)){
            			StoreProgram.getStoreProgram().setiScenario(2);
            		}
            		if(inBounds(event, 470 - posX, 60, 220, 130)){
            			StoreProgram.getStoreProgram().setiScenario(3);
            		}
            		if( inBounds(event, 5, 240, g.getWidth() - 10, 40)){
                    	game.onBackPressed();
                    }
            	}else{
	            	posX += (event.x - touchedX);
	            	isMoving = false;
	           	}
            }
            if( posX > MAX_POSX )
            	posX = MAX_POSX;
            if ( posX < MIN_POSX)
            	posX = MIN_POSX;
        }

	}

	@Override
	public void present(float deltaTime) {
		Graphics g = game.getGraphics();
		
		g.drawPixmap(Assets2D.background, 0, 0);
		
		g.drawPixmap(Assets2D.imgScenario01, 10 - posX, 60);
		g.drawPixmap(Assets2D.imgScenario02, 240 - posX, 60);
		g.drawPixmap(Assets2D.imgScenario03, 470 - posX, 60);
		
		if(StoreProgram.getStoreProgram().getiScenario() == 1){
			g.drawPixmap(Assets2D.imgSelectedScenario, 10  - posX, 60);
		}
		else if(StoreProgram.getStoreProgram().getiScenario() == 2){
			g.drawPixmap(Assets2D.imgSelectedScenario, 240  - posX, 60);
		}
		else if(StoreProgram.getStoreProgram().getiScenario() == 3){
			g.drawPixmap(Assets2D.imgSelectedScenario, 470  - posX, 60);
		}
		
		g.drawText("Selecione o cenario", 10, 40, 30, Color.BLACK);
		g.drawRect(5, 240, g.getWidth() - 10, 40, Color.GRAY);
		g.drawText("OK", 225, 270, 30, Color.WHITE);
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
    	if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
    			return true;
        else
                return false;
    }
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
