package ipa.ezequiel.simprogrobots.Screen3d;

import ipa.ezequiel.simprogrobots.Screen3d.Assets3D;
import ipa.ezequiel.simprogrobots.Screen3d.SimulationScreen;
import ipa.ezequiel.simprogrobots.framework.Screen;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MainSimulation extends GLGame {
	boolean firstTimeCreate = true;

	@Override
	public Screen getStartScreen() {
		return new SimulationScreen(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		super.onSurfaceCreated(gl, config);
		if (firstTimeCreate) {
			Assets3D.load(this);
			firstTimeCreate = false;
		} else {
			Assets3D.reload();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}
}
