package ipa.ezequiel.simprogrobots.Screen3d;

import ipa.ezequiel.simprogrobots.framework.DynamicGameObject3D;
import ipa.ezequiel.simprogrobots.framework.program.Program;

public class Robot extends DynamicGameObject3D{

	private static float ROBOT_RADIUS = 0.5f;
	
	private Program program;
	
	
	public Robot( float x, float y, float z){
		super( x, y, z, ROBOT_RADIUS );
		
		program = new Program();
	}
	

	public void update(float deltaTime) {
		
		if( program.isRunning() ){
			//program.runProgram(deltaTime, this.position, this.orientation ); 
			//Log.d("LOG" ,"Position " + this.position.z );
		}
	}
	
}
