package ipa.ezequiel.simprogrobots.Screen3d;

import java.util.List;

import ipa.ezequiel.simprogrobots.AndroidGame;
import ipa.ezequiel.simprogrobots.Screen3d.World.WorldListener;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.framework.gl.Camera2D;
import ipa.ezequiel.simprogrobots.framework.gl.FPSCounter;
import ipa.ezequiel.simprogrobots.framework.gl.SpriteBatcher;
import ipa.ezequiel.simprogrobots.framework.impl.GLScreen;
import ipa.ezequiel.simprogrobots.framework.math.OverlapTester;
import ipa.ezequiel.simprogrobots.framework.math.Rectangle;
import ipa.ezequiel.simprogrobots.framework.math.Vector2;

import javax.microedition.khronos.opengles.GL10;

public class SimulationScreen extends GLScreen {
	static final int GAME_RUNNING = 0;
	static final int GAME_PAUSED = 1;
	static final int GAME_OVER = 2;

	static final int SIMULATION_RUNNING = 0;
	static final int SIMULATION_PAUSED  = 1;
	static final int SIMULATION_LOAD    = 2;
	
	int state;
	Camera2D guiCam;
	Vector2 touchPoint;
	SpriteBatcher batcher;
	World world;
	WorldListener worldListener;
	WorldRenderer renderer;
	Rectangle pauseBounds;
	Rectangle resumeBounds;
	Rectangle quitBounds;
	Rectangle leftBounds;
	Rectangle rightBounds;
	Rectangle shotBounds;
	int lastScore;
	int lastLives;
	int lastWaves;
	String scoreString;
	FPSCounter fpsCounter;
	InformationsLog information;
	
	public SimulationScreen(AndroidGame game) {
		super(game);

		state = GAME_RUNNING;
		guiCam = new Camera2D(glGraphics, 480, 320);
		touchPoint = new Vector2();
		batcher = new SpriteBatcher(glGraphics, 100);
		
		world = new World();
		worldListener = new WorldListener() {
			@Override
			public void shot() {
			}

			@Override
			public void explosion() {
			}
		};
		
		world.setWorldListener(worldListener);
		renderer = new WorldRenderer(glGraphics);
		pauseBounds = new Rectangle(480 - 64, 320 - 64, 64, 64);
		resumeBounds = new Rectangle(240 - 80, 160, 160, 32);
		quitBounds = new Rectangle(240 - 80, 160 - 32, 160, 32);
		
		shotBounds = new Rectangle(480 - 64, 0, 64, 64);
		leftBounds = new Rectangle(0, 0, 64, 64);
		rightBounds = new Rectangle(64, 0, 64, 64);
		
		/*** APP INFORMATIONS ***/
		fpsCounter = new FPSCounter();
		information = new InformationsLog( renderer.camera.getPosition(), renderer.camera.getLookAt());
	}

	@Override
	public void update(float deltaTime) {
		
		switch (state) {
		case SIMULATION_LOAD:
			break;
		
		case SIMULATION_RUNNING:
			updateRunning(deltaTime);
			break;
		
		case SIMULATION_PAUSED:
			break;
		
		}	
	}

	@SuppressWarnings("unused")
	private void updatePaused() {
		List<TouchEvent> events = game.getInput().getTouchEvents();
		int len = events.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = events.get(i);
			if (event.type != TouchEvent.TOUCH_UP)
				continue;

			guiCam.touchToWorld(touchPoint.set(event.x, event.y));
			if (OverlapTester.pointInRectangle(resumeBounds, touchPoint)) {
				//Assets3D.playSound(Assets3D.clickSound);
				state = GAME_RUNNING;
			}

			if (OverlapTester.pointInRectangle(quitBounds, touchPoint)) {
				//Assets3D.playSound(Assets3D.clickSound);
				//game.setScreen(new MainMenuScreen(game));
			}
		}
	}

	private void updateRunning(float deltaTime) {
		List<TouchEvent> events = game.getInput().getTouchEvents();
		int len = events.size();
		
		for (int i = 0; i < len; i++) {
			TouchEvent event = events.get(i);
			if (event.type != TouchEvent.TOUCH_DOWN)
				continue;

			guiCam.touchToWorld(touchPoint.set(event.x, event.y));

			if (OverlapTester.pointInRectangle(pauseBounds, touchPoint)) {
				state = GAME_PAUSED;
			}
			//Verifica se o p
			if (OverlapTester.pointInRectangle(shotBounds, touchPoint)) {
				//world.ship.position.z--;
			}
		}

		//world.update(deltaTime, calculateInputAcceleration());
		world.update(deltaTime);
		
	}

	@SuppressWarnings("unused")
	private void updateGameOver() {
		List<TouchEvent> events = game.getInput().getTouchEvents();
		int len = events.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = events.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				//game.setScreen(new MainMenuScreen(game));
			}
		}
	}

	@Override
	public void present(float deltaTime) {
		GL10 gl = glGraphics.getGL();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		guiCam.setViewportAndMatrices();

		
		gl.glEnable(GL10.GL_TEXTURE_2D);
		batcher.beginBatch(Assets3D.background);
		batcher.drawSprite(240, 160, 480, 320, Assets3D.backgroundRegion);
		batcher.endBatch();
		gl.glDisable(GL10.GL_TEXTURE_2D);
		
		
		renderer.render(world, deltaTime);

		switch (state) {
		case GAME_RUNNING:
			presentRunning();
			break;
		case GAME_OVER:
			presentGameOver();
		}

		fpsCounter.logFrame();
		information.log();
		
	}

	@SuppressWarnings("unused")
	private void presentPaused() {
		
		GL10 gl = glGraphics.getGL();
		guiCam.setViewportAndMatrices();
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL10.GL_TEXTURE_2D);

		batcher.beginBatch(Assets3D.items);
		Assets3D.font.drawText(batcher, scoreString, 10, 320 - 20);
		batcher.drawSprite(240, 160, 160, 64, Assets3D.pauseRegion);
		batcher.endBatch();

		gl.glDisable(GL10.GL_TEXTURE_2D);
		gl.glDisable(GL10.GL_BLEND);
	
	}

	private void presentRunning() {
	
		GL10 gl = glGraphics.getGL();
		guiCam.setViewportAndMatrices();
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL10.GL_TEXTURE_2D);

		batcher.beginBatch(Assets3D.items);
		batcher.drawSprite(480 - 32, 320 - 32, 64, 64, Assets3D.pauseButtonRegion);
		//ts3D.font.drawText(batcher, scoreString, 10, 320 - 20);
		if (true) {
			batcher.drawSprite(32, 32, 64, 64, Assets3D.leftRegion);
			batcher.drawSprite(96, 32, 64, 64, Assets3D.rightRegion);
		}
		batcher.drawSprite(480 - 40, 32, 64, 64, Assets3D.fireRegion);
		batcher.endBatch();

		gl.glDisable(GL10.GL_TEXTURE_2D);
		gl.glDisable(GL10.GL_BLEND);
	
	}

	private void presentGameOver() {
		GL10 gl = glGraphics.getGL();
		guiCam.setViewportAndMatrices();
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL10.GL_TEXTURE_2D);

		batcher.beginBatch(Assets3D.items);
		batcher.drawSprite(240, 160, 128, 64, Assets3D.gameOverRegion);
		Assets3D.font.drawText(batcher, scoreString, 10, 320 - 20);
		batcher.endBatch();

		gl.glDisable(GL10.GL_TEXTURE_2D);
		gl.glDisable(GL10.GL_BLEND);
	}

	@Override
	public void pause() {
		state = GAME_PAUSED;
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}
}
