package ipa.ezequiel.simprogrobots.Screen3d;

import java.util.Random;

public class World {

	public interface WorldListener {
		public void explosion();
		public void shot();
	}

	public final static float WORLD_MIN_X = -100;
	public final static float WORLD_MAX_X = 100;
	public final static float WORLD_MIN_Z = -15;

	/*
	 * Para testar o 3d
	 */
	int stateCamera = 0;
	
	WorldListener listener;
	int waves = 1;
	int score = 0;
	float speedMultiplier = 1;
	
	Random random;

	final Robot robot;
	long lastTime;
	long totalTime;
	
	
	public World() {
		robot = new Robot( 0, 0, 0);
		random = new Random();
		
		/* Inicializa variaveis de tempo */
		lastTime = 0;
		totalTime = 0;
	
	}

	public void setWorldListener(WorldListener worldListener) {
		this.listener = worldListener;
	}

	public void update(float deltaTime, float accelX) {

	}

	public void update(float deltaTime) {
		this.robot.update(deltaTime);
	}
	
}
