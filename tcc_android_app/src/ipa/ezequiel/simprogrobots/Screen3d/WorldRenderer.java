package ipa.ezequiel.simprogrobots.Screen3d;

import java.util.List;

import ipa.ezequiel.simprogrobots.framework.gl.AmbientLight;
import ipa.ezequiel.simprogrobots.framework.gl.Animation;
import ipa.ezequiel.simprogrobots.framework.gl.DirectionalLight;
import ipa.ezequiel.simprogrobots.framework.gl.LookAtCamera;
import ipa.ezequiel.simprogrobots.framework.gl.SpriteBatcher;
import ipa.ezequiel.simprogrobots.framework.gl.TextureRegion;
import ipa.ezequiel.simprogrobots.framework.impl.GLGraphics;
import ipa.ezequiel.simprogrobots.framework.math.Vector3;

import javax.microedition.khronos.opengles.GL10;

public class WorldRenderer {
	GLGraphics glGraphics;
	LookAtCamera camera;
	AmbientLight ambientLight;
	DirectionalLight directionalLight;
	SpriteBatcher batcher;
	float invaderAngle = 0;

	public WorldRenderer(GLGraphics glGraphics) {
		
		this.glGraphics = glGraphics;
		camera = new LookAtCamera(67, glGraphics.getWidth()
				/ (float) glGraphics.getHeight(), 0.1f, 100);
		
		camera.getPosition().set(0, 6, 2);
		camera.getLookAt().set(0, 0, -4);

		
		ambientLight = new AmbientLight();
		ambientLight.setColor(0.2f, 0.2f, 0.2f, 1.0f);
		
		directionalLight = new DirectionalLight();
		directionalLight.setDirection(-1, -0.5f, 0);
		
		batcher = new SpriteBatcher(glGraphics, 10);
		
	}

	public void render(World world, float deltaTime) {
		GL10 gl = glGraphics.getGL();
		
		//camera.getLookAt().x = world.ship.position.x;
		//camera.getPosition().x = world.ship.position.x;

		/*
		 * switch(world.stateCamera){
		 
		case 0:
			camera.getPosition().x = world.ship.position.x;
			break;
		case 1:
			camera.getPosition().y = world.ship.position.x;
			break;
		case 2:
			camera.getPosition().z = world.ship.position.x;
			break;
		case 3:
			camera.getLookAt().x = world.ship.position.x;
			break;
		case 4:
			camera.getLookAt().y = world.ship.position.x;
			break;
		case 5:
			camera.getLookAt().z = world.ship.position.x;
			break;
		}*/
		
		//camera.getPosition().x = world.ship.position.x;
		
		camera.setMatrices(gl);
		
		gl.glEnable(GL10.GL_DEPTH_TEST);
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glEnable(GL10.GL_LIGHTING);					
		gl.glEnable(GL10.GL_COLOR_MATERIAL);
//		ambientLight.enable(gl);
		directionalLight.enable(gl, GL10.GL_LIGHT0);

		
		renderCenario(gl);
		renderRobot(gl, world.robot);
		//renderShip(gl, world.ship);
		//renderInvaders(gl, world.invaders, deltaTime);

		gl.glDisable(GL10.GL_TEXTURE_2D);

		gl.glDisable(GL10.GL_COLOR_MATERIAL);
		gl.glDisable(GL10.GL_LIGHTING);
		gl.glDisable(GL10.GL_DEPTH_TEST);		
	}

	private void renderRobot(GL10 gl, Robot robot) {
		Assets3D.shipTexture.bind();
		Assets3D.shipModel.bind();
		
		gl.glPushMatrix();
		
		gl.glTranslatef(robot.position.x, robot.position.y, robot.position.z);
		gl.glRotatef( robot.orientation[0] , 0 , 1, 0);
		
		Assets3D.shipModel.draw(GL10.GL_TRIANGLES, 0,
				Assets3D.shipModel.getNumVertices());
		gl.glPopMatrix();
		Assets3D.shipModel.unbind();
	}

	private void renderCenario(GL10 gl) {
		
		Assets3D.cenarioTexture.bind();
		Assets3D.cenario.bind();
		
		gl.glPushMatrix();
		gl.glTranslatef(0, 0, 0);
		Assets3D.cube.draw(GL10.GL_TRIANGLES, 0, Assets3D.cube.getNumVertices());
		gl.glPopMatrix();
		
		Assets3D.cenario.unbind();
		
	}
	
	@SuppressWarnings("unused")
	private void renderShip(GL10 gl, Ship ship) {
		if (ship.state == Ship.SHIP_EXPLODING) {
			gl.glDisable(GL10.GL_LIGHTING);
			renderExplosion(gl, ship.position, ship.stateTime);
			gl.glEnable(GL10.GL_LIGHTING);
		} else {
			Assets3D.shipTexture.bind();
			Assets3D.shipModel.bind();
			gl.glPushMatrix();
			gl.glTranslatef(ship.position.x, ship.position.y, ship.position.z);
			gl.glRotatef(ship.velocity.x / Ship.SHIP_VELOCITY * 90, 0, 0, -1);
			Assets3D.shipModel.draw(GL10.GL_TRIANGLES, 0,
					Assets3D.shipModel.getNumVertices());
			gl.glPopMatrix();
			Assets3D.shipModel.unbind();
		}
	}

	@SuppressWarnings("unused")
	private void renderShields(GL10 gl, List<Shield> shields) {
		gl.glEnable(GL10.GL_BLEND);
		
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);		
		gl.glColor4f(0, 0, 1, 0.4f);
		
		Assets3D.shieldModel.bind();
		int len = shields.size();
		for (int i = 0; i < len; i++) {
			Shield shield = shields.get(i);
			gl.glPushMatrix();
			gl.glTranslatef(shield.position.x, shield.position.y,
					shield.position.z);
			Assets3D.shieldModel.draw(GL10.GL_TRIANGLES, 0,
					Assets3D.shieldModel.getNumVertices());
			gl.glPopMatrix();
		}
		Assets3D.shieldModel.unbind();
		gl.glColor4f(1, 1, 1, 1f);		
		gl.glDisable(GL10.GL_BLEND);
	}
	
	@SuppressWarnings("unused")
	private void renderShots(GL10 gl, List<Shot> shots) {
		gl.glColor4f(1, 1, 0, 1);
		Assets3D.shotModel.bind();
		int len = shots.size();
		for (int i = 0; i < len; i++) {
			Shot shot = shots.get(i);
			gl.glPushMatrix();
			gl.glTranslatef(shot.position.x, shot.position.y, shot.position.z);
			Assets3D.shotModel.draw(GL10.GL_TRIANGLES, 0,
					Assets3D.shotModel.getNumVertices());
			gl.glPopMatrix();
		}
		Assets3D.shotModel.unbind();
		gl.glColor4f(1, 1, 1, 1);
	}

	private void renderExplosion(GL10 gl, Vector3 position, float stateTime) {
		TextureRegion frame = Assets3D.explosionAnim.getKeyFrame(stateTime,
				Animation.ANIMATION_NONLOOPING);
		
		gl.glEnable(GL10.GL_BLEND);
		gl.glPushMatrix();
		gl.glTranslatef(position.x, position.y, position.z);		
		batcher.beginBatch(Assets3D.explosionTexture);
		batcher.drawSprite(0, 0, 2, 2, frame);
		batcher.endBatch();
		gl.glPopMatrix();
		gl.glDisable(GL10.GL_BLEND);
	}


}