package ipa.ezequiel.simprogrobots;

import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.framework.Screen;

import java.io.IOException;
import java.util.List;

import android.graphics.Color;

public class SendScreen extends Screen {

	private boolean hasSendCommands;

	public SendScreen(AndroidGame game) {
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		//Graphics g = game.getGraphics();
        
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        
    	game.getInput().getKeyEvents();       
        
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
            	if(inBounds(event, 10, 0, 74, 64)) {
                	if( BluetoothControler.getBluetooth().isConnected() ){
                		BluetoothControler.getBluetooth().disableBluetooth();
                	}else{
                		game.setScreenBluetooth();
                	}
                }
            	//Enviar
            	else if(inBounds(event, 260, 30, 210, 50)){
        			//Adicionar Comandos
        		}
        		//Controlar
            	else if(inBounds(event, 260,90,210,40)){
        			
            		if( !hasSendCommands )
            			mountComands();
        			
        			sendCommandToInitiate();
        		}
            	
            }
        }

	}
	
	private void sendCommandToInitiate() {
		
		if( BluetoothControler.getBluetooth().isConnected() ){
			try {
				BluetoothControler.getBluetooth().beginListenForData();
				BluetoothControler.getBluetooth().sendData("SP|F45|E090|F55|D090|F30|SF|I");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
			
	}

	private void mountComands() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void present(float deltaTime) {
		Graphics g = game.getGraphics();
		
		g.drawPixmap(Assets2D.background, 0, 0);
		
		
		if( BluetoothControler.getBluetooth().isConnected()){
			g.drawPixmap(Assets2D.imgOnBluetooth, 10, 0);
			g.drawText("PELEBOT", 75, 40, 25, Color.BLACK);

		}
		else{
			g.drawPixmap(Assets2D.imgOffBluetooth, 10, 0);
			g.drawText("Não Conectado", 75, 40, 25, Color.BLACK);		
		}
		
		mountReturn(g);
		
		//Enviar
		g.drawRect(260, 30, 210, 50, Color.GRAY);
		g.drawText("Enviar", 325, 70, 35, Color.BLACK);

		//Iniciar
		g.drawRect(260, 90, 210, 40, Color.GRAY);
		g.drawText("Iniciar", 335, 120, 25, Color.BLACK);

		
		//Options
		g.drawRect(260, 150, 210, 160, Color.WHITE);
	}

	private void mountReturn(Graphics g) {
		
		//Draw Box of last executed Box
		g.drawPixmap(Assets2D.imgBorder, 30, 125);
		g.drawText("Anterior", 40, 228, 20, Color.BLACK);
		if( BluetoothControler.getBluetooth().receivedData.size() > 0){
			String stringActual = BluetoothControler.getBluetooth().receivedData.get(BluetoothControler.getBluetooth().receivedData.size() - 1);
			showIcon(g, stringActual, 140, 125);
		}	
		
		//Draw Box of in Execution Task
		g.drawPixmap(Assets2D.imgBorder, 140, 125);
		g.drawText("Atual", 160, 228, 20, Color.BLACK);
		if( BluetoothControler.getBluetooth().receivedData.size() > 1){
			String stringLast = BluetoothControler.getBluetooth().receivedData.get(BluetoothControler.getBluetooth().receivedData.size() - 2);
			showIcon(g, stringLast, 30, 125);
		}	
		
		
	}

	private void showIcon(Graphics g, String value, int i, int j) {
		
		if(value.charAt(0) == 'F'){
			g.drawPixmap(Assets2D.imgFront, i, j);
		}
		else if(value.charAt(0) == 'T'){
			g.drawPixmap(Assets2D.imgBack, i, j);
		}
		else if(value.charAt(0) == 'E'){
			g.drawPixmap(Assets2D.imgLeft, i, j);
		}
		else if(value.charAt(0) == 'D'){
			g.drawPixmap(Assets2D.imgRight, i, j);
		}
		
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
    	if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
    			return true;
        else
                return false;
    }
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
}
