package ipa.ezequiel.simprogrobots;

import java.util.List;

import android.graphics.Color;
import ipa.ezequiel.simprogrobots.framework.Graphics;
import ipa.ezequiel.simprogrobots.framework.Screen;
import ipa.ezequiel.simprogrobots.framework.Input.TouchEvent;
import ipa.ezequiel.simprogrobots.framework.program.Program;
import ipa.ezequiel.simprogrobots.Assets2D;



public class SimScreen extends Screen {

	private boolean isRunning;
	private int posX,posY;
	private char cSentido;
	
	static final float TICK_INITIAL = 0.033f;
	float tickTime = 0;
	
	public SimScreen(AndroidGame game) {
		super(game);
		isRunning = false;
		new Program();
		
		posX = game.getGraphics().getWidth()  / 2;
		posY = game.getGraphics().getHeight() / 2;
		cSentido = 'E'; 
				
		
		
	}

	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
        
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        
    	game.getInput().getKeyEvents();       
        
    	tickTime += deltaTime;
    	
    	if(isRunning && tickTime > TICK_INITIAL ){
    		tickTime -= TICK_INITIAL;
    		
    		if( cSentido == 'E' ){
    			if( posX < 100 )
    				cSentido = 'B';
    			posX--;
    		}
    		else if( cSentido == 'B' ){
    			if( posY > 200 )
    				cSentido = 'D';
    			posY++;
    		}
    		else if( cSentido == 'D' ){
    			if( posX < 200 )
    				cSentido = 'C';
    			posX++;
    		}
    		else if( cSentido == 'C' ){
    			if( posY < 100 )
    				cSentido = 'E';
    			posY--;
    		}
    		
    	}
    	
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                /*
            	if(inBounds(event, 0, 0, 15, 15)) {
                	game.setScreen( new MainMenuRobot(game));
                	return;
                }
                */
            	if(inBounds(event, g.getWidth() - 80 , 20 , 70, 70)) {
                	//game.setScreen( new MainMenuRobot(game));
            		isRunning = true;
                	return;
                }
            	else if(inBounds(event, g.getWidth() - 80 , 100 , 70, 70)) {
                	//game.setScreen( new MainMenuRobot(game));
                	isRunning = false;
            		return;
                }
            }
        }

	}

	@Override
	public void present(float deltaTime) {
		Graphics g = game.getGraphics();
		
		g.drawPixmap(Assets2D.background, 0, 0);
		
		g.drawLine( g.getWidth() - 100 , 0, g.getWidth() - 100 , g.getHeight() , Color.LTGRAY );
        
		g.drawRect( g.getWidth() - 80 , 20 , 70, 70, Color.GREEN );
		g.drawRect( g.getWidth() - 80 , 100, 70, 70, Color.RED   );
		
		g.drawRect( posX, posY, 15, 15, Color.YELLOW);
       
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
    	if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
    			return true;
        else
                return false;
    }
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
