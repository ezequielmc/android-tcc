package ipa.ezequiel.simprogrobots.Simulation;

import ipa.ezequiel.simprogrobots.framework.impl.AndroidGraphics;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.util.FloatMath;
import android.view.MotionEvent;
import min3d.core.Object3d;
import min3d.core.Object3dContainer;
import min3d.core.RendererActivity;
import min3d.objectPrimitives.SkyBox;
import min3d.parser.IParser;
import min3d.parser.Parser;
import min3d.vos.Light;
import min3d.vos.Number3d;

public class CopyOfSimulationScreen extends RendererActivity 
{
	private final float MAX_ROTATION = 40;
	private final float MAX_CAM_X = 6f;
	private Object3dContainer car;
	private Object3d tireRR;
	private Object3d tireRF;
	private Object3d tireLR;
	private Object3d tireLF;
	private int rotationDirection;
	private float camDirection;
	private SkyBox mSkyBox;
	private float touchedX;
	private float touchedY;
	private Canvas canvas;
	private AndroidGraphics graphics;
	
	@Override
	public void initScene() {

		
		//Canvas
		 int frameBufferWidth = 480;
	        int frameBufferHeight = 32;
	        
	        Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
	                frameBufferHeight, Config.RGB_565);
	    
	   this.graphics = new AndroidGraphics(getAssets(), frameBuffer);
		
		
		mSkyBox = new SkyBox(50.0f, 2);
		mSkyBox.addTexture(SkyBox.Face.North, 	ipa.ezequiel.simprogrobots.R.drawable.wood_back, 	"north");
		mSkyBox.addTexture(SkyBox.Face.East, 	ipa.ezequiel.simprogrobots.R.drawable.wood_right, 	"east");
		mSkyBox.addTexture(SkyBox.Face.South, 	ipa.ezequiel.simprogrobots.R.drawable.wood_back, 	"south");
		mSkyBox.addTexture(SkyBox.Face.West, 	ipa.ezequiel.simprogrobots.R.drawable.wood_left, 	"west");
		mSkyBox.addTexture(SkyBox.Face.Up,		ipa.ezequiel.simprogrobots.R.drawable.ceiling, 		"up");
		mSkyBox.addTexture(SkyBox.Face.Down, 	ipa.ezequiel.simprogrobots.R.drawable.floor, 		"down");
		mSkyBox.scale().y = 2.8f;
		mSkyBox.scale().z = 4.0f;
		scene.addChild(mSkyBox);

		IParser parser = Parser.createParser(Parser.Type.OBJ,
				getResources(), "ipa.ezequiel.simprogrobots:raw/carblender_obj", true);
		parser.parse();

		car = parser.getParsedObject();

		scene.addChild(car);
		
		/*
		tireRR = car.getChildByName("tire_rr");
		tireRF = car.getChildByName("tire_rf");
		tireLR = car.getChildByName("tire_lr");
		tireLF = car.getChildByName("tire_lf");

		tireLF.position().x = -.6f;
		tireLF.position().y = 1.11f;
		tireLF.position().z = .3f;
		
		tireRF.position().x = .6f;
		tireRF.position().y = 1.11f;
		tireRF.position().z = .3f;
		
		tireRR.position().x = .6f;
		tireRR.position().y = -1.05f;
		tireRR.position().z = .3f;

		tireLR.position().x = -.6f;
		tireLR.position().y = -1.05f;
		tireLR.position().z = .3f;
		 */
		
		
		//car.rotation().x = -90;
		//car.rotation().z = 180;
		
		
		scene.camera().position.x = MAX_CAM_X;
		scene.camera().position.z = -3.5f;
		scene.camera().position.y = -3.5f;
		

		Light light = new Light();
		light.position.setAllFrom(scene.camera().position);
		scene.lights().add(light);
		
		rotationDirection = 1;
		camDirection = -.01f;
	}

	
	
	@Override
	public void updateScene(float deltaTime) {
		/*
		tireRF.rotation().z += rotationDirection;
		tireLF.rotation().z += rotationDirection;
		
		if(Math.abs(tireRF.rotation().z) >= MAX_ROTATION)
			rotationDirection = -rotationDirection;
		*/
		
		//scene.camera().position.x += camDirection;
		//scene.lights().get(0).position.setAllFrom(scene.camera().position);
		
		if(Math.abs(scene.camera().position.x) >= MAX_CAM_X)
			camDirection = -camDirection;
		
		graphics.drawRect(0, 0, 20, 20, Color.BLUE);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event){	
		
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			touchedX = event.getX();
			touchedY = event.getY();
			
			
		} else if (event.getAction() == MotionEvent.ACTION_MOVE)
		{
			car.rotation().x -= (touchedY - event.getY())/2f;
			car.rotation().z -= (touchedX - event.getX())/2f;
					
			//car.rotation().z -= (touchedX - event.getX())/2f;
			//car.rotation().x -= (touchedY - event.getY())/2f;
			
			
			touchedX = event.getX();
			touchedY = event.getY();
			
			
		}       	
		return true;	
	}

	private float spacing(MotionEvent event) {
		   float x = event.getX(0) - event.getX(1);
		   float y = event.getY(0) - event.getY(1);
		   return FloatMath.sqrt(x * x + y * y);
		}

}