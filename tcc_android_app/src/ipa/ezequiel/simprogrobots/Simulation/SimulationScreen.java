package ipa.ezequiel.simprogrobots.Simulation;

import ipa.ezequiel.simprogrobots.framework.program.Program;
import ipa.ezequiel.simprogrobots.framework.program.StoreProgram;
import ipa.ezequiel.simprogrobots.framework.program.guiChanges;
import min3d.core.Object3d;
import min3d.core.Object3dContainer;
import min3d.core.RendererActivity;
import min3d.objectPrimitives.SkyRect;
import min3d.parser.IParser;
import min3d.parser.Parser;
import min3d.vos.Light;
import min3d.vos.Number3d;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Example of adding an OpenGL scene within a conventional Android application layout.
 * Entails overriding RenderActivity's onCreateSetContentView() function, and
 * adding _glSurfaceView to the appropriate View...  
 * 
 * @author Lee
 */

public class SimulationScreen extends RendererActivity 
{
	Object3dContainer _robot;
	Object3d _tireLeftFront;
	Object3d _tireRightFront;
	private Object3dContainer _tireLeftBack;
	private Object3dContainer _tireRightBack;
	
	SkyRect skyRect;
	
	Number3d position;
	Number3d rotation;
	
	private float touchedY;
	private float touchedX;
	
	Button bStartPause;
	Button bRestart;
	Button b2d;
	Button b3d;
	ImageView iViewExecution;
	TextView showValue1;
	
	boolean state;
	private Program program;
	
	//Follow Camera
	private float cameraDistance = 25f;
	
	private int selectedCamera = 1;
	private Object3dContainer _sensorDist;
	private Light _lightRed;
	private Number3d lastposition;
	private Number3d lastrotation;
	

	@Override
	protected void onCreateSetContentView()
	{
		setContentView(ipa.ezequiel.simprogrobots.R.layout.custom_layout_example);
		
        LinearLayout ll = (LinearLayout) this.findViewById(ipa.ezequiel.simprogrobots.R.id.scene1Holder);
        ll.addView(_glSurfaceView);
        
        state = false;
        program = new Program();
        
        iViewExecution = (ImageView) this.findViewById(ipa.ezequiel.simprogrobots.R.id.image_execution);
        showValue1 = (TextView) this.findViewById(ipa.ezequiel.simprogrobots.R.id.showValue1);
        
        bStartPause = (Button) this.findViewById(ipa.ezequiel.simprogrobots.R.id.bStartPause);
        bStartPause.setOnClickListener( new View.OnClickListener() {
             public void onClick(View v) {
                 if( program.isRunning() ){
                	 program.stopProgram();
                	 //button.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
                	 bStartPause.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable(ipa.ezequiel.simprogrobots.R.drawable.play), null, null, null);
                 }
                 else {
                	 if(program.isFinished()){
                		 positionateRobot();
                     }
                	 program.startRunningProgram();
                	 bStartPause.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable(ipa.ezequiel.simprogrobots.R.drawable.pause), null, null, null);
                 }
             }
         });
	    
        
        bRestart = (Button) this.findViewById(ipa.ezequiel.simprogrobots.R.id.bRestart);
        bRestart.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
            	positionateRobot();
            	program.startRunningProgram();
                bStartPause.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable(ipa.ezequiel.simprogrobots.R.drawable.play), null, null, null);
                
            }
        });
        
        b2d = (Button) this.findViewById(ipa.ezequiel.simprogrobots.R.id.Button2d);
        b2d.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
            	selectedCamera = 2;
            }
        });
        
        b3d = (Button) this.findViewById(ipa.ezequiel.simprogrobots.R.id.Button3d);
        b3d.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
            	selectedCamera = 1;
            }
        });
	    
	}

    @Override
	public boolean onTouchEvent(MotionEvent event){	
		
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			touchedX = event.getX();
			touchedY = event.getY();
			
			
		} else if (event.getAction() == MotionEvent.ACTION_MOVE)
		{
			//_robot.position().y += (touchedX - event.getX())/12f;
			//scene.camera().position.y -= (event.getX() - touchedX);
			//	_sensorDist.position().y -= (event.getX() - touchedX);
			
			touchedX = event.getX();
			touchedY = event.getY();
			Log.d("OI", "Valor = " +  _robot.position().z );
		}       	
		return true;	
	}
	
	public void initScene() 
	{
		scene.lights().add(new Light());
		scene.backgroundColor().setAll(0xffffffff);
		//_robot = new Box(1,1,1);
		scene.fogEnabled(false);
		
		loadRobot();
		
		loadScenario();
		//Para atualizar a tela
		updateCamera();
	}

	private void loadScenario() {
		
		
		skyRect = new SkyRect( getResources(), 8f, 11.0f, 5.0f, 2);
		skyRect.addTexture(SkyRect.Face.North , 	ipa.ezequiel.simprogrobots.R.drawable.wood_back , 	"north");
		skyRect.addTexture(SkyRect.Face.East  , 	ipa.ezequiel.simprogrobots.R.drawable.wood_right , 	"east");
		skyRect.addTexture(SkyRect.Face.South , 	ipa.ezequiel.simprogrobots.R.drawable.wood_back, 	"south");
		skyRect.addTexture(SkyRect.Face.West  , 	ipa.ezequiel.simprogrobots.R.drawable.wood_left, 	"west");
		
		if(StoreProgram.getStoreProgram().getiScenario() == 1){
			skyRect.addTexture(SkyRect.Face.Down  ,  ipa.ezequiel.simprogrobots.R.drawable.scenario01_2048 , 	"down");
		}
		else if(StoreProgram.getStoreProgram().getiScenario() == 2){
			skyRect.addTexture(SkyRect.Face.Down  ,  ipa.ezequiel.simprogrobots.R.drawable.scenario02, 	"down");
		}  
		    
		else if(StoreProgram.getStoreProgram().getiScenario() == 3	){
			skyRect.addTexture(SkyRect.Face.Down  , 	ipa.ezequiel.simprogrobots.R.drawable.scenario03, 	"down");
		
			//Load Cube
		    String value = "ipa.ezequiel.simprogrobots:raw/cube_obj";
			IParser parser = Parser.createParser(Parser.Type.OBJ,
					getResources(), value, true);
		    parser.parse();
			
		    Object3dContainer cube = parser.getParsedObject();
		    cube.scale().z = 6f;
		    cube.scale().y = 7.5f;
		    cube.scale().x = 34f;
		    
		    cube.position().x = 22f;
		    cube.position().z = -6f;
		    
		    scene.addChild(cube);
		    
		}
		
		scene.addChild(skyRect);

	
		skyRect.scale().z = 10f;
		skyRect.scale().x = 10f;
	}

	private void loadRobot() {
		String value;
	
	    if( StoreProgram.getStoreProgram().getiRobot() == 1 ){
	    	value = "ipa.ezequiel.simprogrobots:raw/alcalabot_obj";
		}
		else{
			value = "ipa.ezequiel.simprogrobots:raw/brinquedo_obj";
		}
		
		IParser parser = Parser.createParser(Parser.Type.OBJ,
				getResources(), value, true);
	    parser.parse();
		
	    _robot = parser.getParsedObject();
		
		if( StoreProgram.getStoreProgram().getiRobot() == 1 ){
	    	value = "ipa.ezequiel.simprogrobots:raw/alacalabot_roda_obj";
	    	parser = Parser.createParser(Parser.Type.OBJ, getResources(), value, true);
		    parser.parse();
		    _tireLeftBack = parser.getParsedObject();
		    _tireRightBack = _tireLeftBack.clone();
		    
		    value = "ipa.ezequiel.simprogrobots:raw/alcalabot_sensor_obj";
	    	parser = Parser.createParser(Parser.Type.OBJ, getResources(), value, true);
		    parser.parse();
		    
		    _sensorDist = parser.getParsedObject();
		    
		    scene.addChild(_tireLeftBack);
		    scene.addChild(_tireRightBack);
		    scene.addChild(_sensorDist);
		     
		}
		else{
			value = "ipa.ezequiel.simprogrobots:raw/roda_brinquedo_obj";
	    	parser = Parser.createParser(Parser.Type.OBJ, getResources(), value, true);
		    parser.parse();
		    _tireLeftBack = parser.getParsedObject();
		    _tireRightBack = _tireLeftBack.clone();
		    _tireLeftFront = _tireLeftBack.clone();
		    _tireRightFront = _tireLeftBack.clone();
		    _sensorDist = new Object3dContainer();
		    
		    scene.addChild(_tireLeftBack);
		    scene.addChild(_tireRightBack);
		    scene.addChild(_tireLeftFront);
		    scene.addChild(_tireRightFront);
		    
		}

	    positionateRobot();

		scene.addChild(_robot);
	   
	}

	private void positionateRobot() {
		
		
		_robot.position().z = 46.5f;
		_robot.position().x = 22.5f;
		_robot.position().y = 0.6f;
		_robot.scale().x = _robot.scale().y = _robot.scale().z = 1.8f;
		_robot.lightingEnabled(false);
		
		if( StoreProgram.getStoreProgram().getiRobot() == 1){
			
			//Antigo valor de z = 36.5f
			_tireLeftBack.position().z = (float) (_robot.position().z + 4 );
			_tireLeftBack.position().x = (float) (_robot.position().x - 3.6f * Math.cos(Math.toRadians(_robot.rotation().y)));
			_tireLeftBack.position().y = 0f;
			_tireLeftBack.rotation().y = 90 + _robot.rotation().y;
			_tireLeftBack.scale().x = _tireLeftBack.scale().y = _tireLeftBack.scale().z = 1.5f;
			
			
			_tireRightBack.position().z = (float) (_robot.position().z +4 );
			_tireRightBack.position().x = (float) (_robot.position().x + 3.6f * Math.cos(Math.toRadians(_robot.rotation().y)));
			_tireRightBack.position().y = 0f;
			_tireRightBack.rotation().y = -90 + _robot.rotation().y;
			_tireRightBack.scale().x = _tireRightBack.scale().y = _tireRightBack.scale().z = 1.5f;
			
			_sensorDist.position().z = _robot.position().z - 1.8f ;
			_sensorDist.position().x = _robot.position().x;
			_sensorDist.position().y = 0.2f;
			_sensorDist.scale().x = _sensorDist.scale().y = _sensorDist.scale().z = 1.5f;
			
		}
		else{
			
			_tireLeftBack.position().z = (float) (_robot.position().z + 4.5 );
			_tireLeftBack.position().x = (float) (_robot.position().x - 3.1f * Math.cos(Math.toRadians(_robot.rotation().y)));
			_tireLeftBack.position().y = .4f;
			_tireLeftBack.rotation().y = 0;
			_tireLeftBack.scale().x = _tireLeftBack.scale().y = _tireLeftBack.scale().z = 1.5f;
			
			
			_tireRightBack.position().z = (float) (_robot.position().z + 4.5 );
			_tireRightBack.position().x = (float) (_robot.position().x + 3.1f * Math.cos(Math.toRadians(_robot.rotation().y)));
			_tireRightBack.position().y = .4f;
			_tireRightBack.rotation().y = 180;
			_tireRightBack.scale().x = _tireRightBack.scale().y = _tireRightBack.scale().z = 1.5f;

			
			_tireLeftFront.position().z = (float) (_robot.position().z - 1 );
			_tireLeftFront.position().x = (float) (_robot.position().x - 3.1f * Math.cos(Math.toRadians(_robot.rotation().y)));
			_tireLeftFront.position().y = .4f;
			_tireLeftFront.rotation().y =  0;
			_tireLeftFront.scale().x = _tireLeftFront.scale().y = _tireLeftFront.scale().z = 1.5f;
			
			_tireRightFront.position().z = (float) (_robot.position().z - 1 );
			_tireRightFront.position().x = (float) (_robot.position().x + 3.1f * Math.cos(Math.toRadians(_robot.rotation().y)));
			_tireRightFront.position().y = .4f; 
			_tireRightFront.rotation().y = 180; 
			_tireRightFront.scale().x = _tireRightFront.scale().y = _tireRightFront.scale().z = 1.5f;
					
		}
	}

	private void actualizeView( final guiChanges a ){
		//TODO: Otimizar perfomance
		runOnUiThread(new Runnable() {  
            @Override
            public void run() {
               iViewExecution.setImageResource(a.getResID());
               if( a.isRunning() )
            	   showValue1.setText(a.getValue1());
               else{
            	   showValue1.setText("");
          	 	   bStartPause.setCompoundDrawablesWithIntrinsicBounds( getResources().getDrawable(ipa.ezequiel.simprogrobots.R.drawable.play), null, null, null);
               }
          }    
       });
	}
	
	@Override 
	public void updateScene(float deltaTime) 
	{
		if( program.isRunning() ){
			
			
			//To make a delta variation.
			lastposition = _robot.position().clone();
			lastrotation = _robot.rotation().clone();
			
			guiChanges a = program.runProgram(deltaTime, _robot.position(), 
							_robot.rotation(), iViewExecution, _sensorDist.rotation() );
			
			
			if( StoreProgram.getStoreProgram().getiRobot() == 1 ){
				
				_tireLeftBack.position().x = (float) (_robot.position().x  - 3.6f * Math.cos(Math.toRadians(_robot.rotation().y)) + 4 * Math.sin(Math.toRadians(_robot.rotation().y)));
				_tireRightBack.position().x = (float) (_robot.position().x + 3.6f * Math.cos(Math.toRadians(_robot.rotation().y)) + 4 * Math.sin(Math.toRadians(_robot.rotation().y)));
				_tireLeftBack.position().z = (float)  (_robot.position().z + 3.6 * Math.sin(Math.toRadians(_robot.rotation().y))  + 4 * Math.cos(Math.toRadians(_robot.rotation().y)));
				_tireRightBack.position().z = (float) (_robot.position().z - 3.6 * Math.sin(Math.toRadians(_robot.rotation().y))  + 4 * Math.cos(Math.toRadians(_robot.rotation().y)));
				
				_tireLeftBack.rotation().y = 90 + _robot.rotation().y;
				_tireRightBack.rotation().y = -90 + _robot.rotation().y;
			
				
				//
				/*htBack.rotation().x += (float) 
				((_tireRightBack.position().x) - lastposition.x ) * Math.sin(Math.toRadians(_robot.rotation().y)) * 9.8 ;
				_tireLeftBack.rotation().x  += (float) 
				((_tireLeftBack.position().x) - lastposition.x ) * Math.sin(Math.toRadians(_robot.rotation().y)) * 9.8 ;
				*/
				_tireRightBack.rotation().z += (float) 
				((_tireRightBack.position().z) - lastposition.z ) ;
				_tireLeftBack.rotation().z  += (float) 
				((_tireLeftBack.position().z) - lastposition.z ) ;

				
				
				_sensorDist.position().z = (float) (_robot.position().z - 1.8 * Math.cos(Math.toRadians(_robot.rotation().y) )   ) ;
				_sensorDist.position().x = (float) (_robot.position().x - 1.8 * Math.sin(Math.toRadians(_robot.rotation().y) )   ) ;

				_sensorDist.rotation().y = ( _sensorDist.rotation().y - lastrotation.y + _robot.rotation().y);
			
			}
			else{

				_tireLeftBack.position().x =   (float) (_robot.position().x - 3.1f * Math.cos(Math.toRadians(_robot.rotation().y)) + 4.5 * Math.sin(Math.toRadians(_robot.rotation().y)));
				_tireRightBack.position().x =  (float) (_robot.position().x + 3.1f * Math.cos(Math.toRadians(_robot.rotation().y)) + 4.5 * Math.sin(Math.toRadians(_robot.rotation().y)));
				_tireLeftFront.position().x =  (float) (_robot.position().x - 3.1f * Math.cos(Math.toRadians(_robot.rotation().y)) -1  * Math.sin(Math.toRadians(_robot.rotation().y)));
				_tireRightFront.position().x = (float) (_robot.position().x + 3.1f * Math.cos(Math.toRadians(_robot.rotation().y)) -1  * Math.sin(Math.toRadians(_robot.rotation().y)));
				
				_tireLeftBack.position().z =   (float)  (_robot.position().z + 3.1 * Math.sin(Math.toRadians(_robot.rotation().y)) + 4.5 * Math.cos(Math.toRadians(_robot.rotation().y)));
				_tireRightBack.position().z =  (float) (_robot.position().z - 3.1 * Math.sin(Math.toRadians(_robot.rotation().y)) + 4.5 * Math.cos(Math.toRadians(_robot.rotation().y)));
				_tireLeftFront.position().z =  (float)  (_robot.position().z + 3.1 * Math.sin(Math.toRadians(_robot.rotation().y)) -1 * Math.cos(Math.toRadians(_robot.rotation().y)));
				_tireRightFront.position().z = (float) (_robot.position().z - 3.1 * Math.sin(Math.toRadians(_robot.rotation().y)) -1 * Math.cos(Math.toRadians(_robot.rotation().y)));
				
				_tireLeftBack.rotation().y = _robot.rotation().y; 
				_tireRightBack.rotation().y = 180 + _robot.rotation().y;
				_tireLeftFront.rotation().y = _robot.rotation().y;
				_tireRightFront.rotation().y = 180 + _robot.rotation().y;
				
				
			}
			actualizeView( a );
			
		}
		updateCamera();
	}

	private void updateCamera() {
		
		if( selectedCamera == 1){
			scene.camera().position.y =  _robot.position().y + 10f;
		    
			scene.camera().position.x = _robot.position().x + (float)Math.sin(Math.toRadians(_robot.rotation().y) ) * cameraDistance ;
			
			scene.camera().position.z = _robot.position().z + (float)Math.cos(Math.toRadians(_robot.rotation().y )) * cameraDistance 
					+ (float) Math.abs( Math.sin(Math.toRadians(_robot.rotation().y) ) * 1.52f );
			
			
			scene.camera().target.x = _robot.position().x;
		    scene.camera().target.y = _robot.position().y;
		    scene.camera().target.z = _robot.position().z;
		}
		else{
			scene.camera().position.y =  50f;
			scene.camera().position.x = _robot.position().x + (float)Math.sin(Math.toRadians(_robot.rotation().y) ) * cameraDistance ;
			scene.camera().position.z = _robot.position().z + (float)Math.cos(Math.toRadians(_robot.rotation().y) ) * cameraDistance 
					+ (float) Math.abs( Math.sin(_robot.rotation().y ) * 1.52f );
			
			scene.camera().target.x = _robot.position().x;
		    scene.camera().target.y = _robot.position().y;
		    scene.camera().target.z = _robot.position().z;
		}
		
	}
	
}

