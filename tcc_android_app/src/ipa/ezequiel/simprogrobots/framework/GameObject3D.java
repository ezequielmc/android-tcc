package ipa.ezequiel.simprogrobots.framework;

import ipa.ezequiel.simprogrobots.framework.math.Sphere;
import ipa.ezequiel.simprogrobots.framework.math.Vector3;

public class GameObject3D {
	public final Vector3 position;
	public int[] orientation ;
    public final Sphere bounds;
    
    public GameObject3D(float x, float y, float z, float radius) {
        this.position = new Vector3(x,y,z);
        this.bounds = new Sphere(x, y, z, radius);
        this.orientation = new int[1];
        this.orientation[0] = 0;
    }
}
