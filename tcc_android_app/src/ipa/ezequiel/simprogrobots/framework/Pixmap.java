package ipa.ezequiel.simprogrobots.framework;

import ipa.ezequiel.simprogrobots.framework.Graphics.PixmapFormat;

public interface Pixmap {
    public int getWidth();

    public int getHeight();

    public PixmapFormat getFormat();

    public void dispose();
}
