package ipa.ezequiel.simprogrobots.framework;

import ipa.ezequiel.simprogrobots.AndroidGame;

public abstract class Screen {
    protected final AndroidGame game;

    public Screen(AndroidGame game) {
        this.game = game;
    }

    public abstract void update(float deltaTime);

    public abstract void present(float deltaTime);

    public abstract void pause();

    public abstract void resume();

    public abstract void dispose();
}
