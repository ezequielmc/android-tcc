package ipa.ezequiel.simprogrobots.framework.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.res.AssetManager;
import android.os.Environment;

import ipa.ezequiel.simprogrobots.framework.FileIO;

public class AndroidFileIO implements FileIO {
    AssetManager assets;
    String externalStoragePath;
    String folder = "ipa.ezequiel.simprogrobots";
    
    
    public AndroidFileIO(AssetManager assets) {
        this.assets = assets;
        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
        		+ this.folder);
		
        if (!folder.exists()) {
		    folder.mkdir();
		}else{
			folder.setReadable(true);
			folder.setWritable(true);
		}
		this.externalStoragePath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + File.separator + this.folder + File.separator;
    }

    @Override
    public InputStream readAsset(String fileName) throws IOException {
        return assets.open(fileName);
    }

    @Override
    public InputStream readFile(String fileName) throws IOException {
        return new FileInputStream(externalStoragePath + fileName);
    }

    @Override
    public OutputStream writeFile(String fileName) throws IOException {
        return new FileOutputStream(externalStoragePath + fileName);
    }
}
