package ipa.ezequiel.simprogrobots.framework.impl;

import ipa.ezequiel.simprogrobots.AndroidGame;
import ipa.ezequiel.simprogrobots.Screen3d.GLGame;
import ipa.ezequiel.simprogrobots.framework.Game;
import ipa.ezequiel.simprogrobots.framework.Screen;

public abstract class GLScreen extends Screen {
    protected final GLGraphics glGraphics;
    protected final GLGame glGame;
    
    public GLScreen(AndroidGame game) {
        super(game);
        Game tmpGame = (Game)game;
        glGame = (GLGame) tmpGame;
        glGraphics = ((GLGame)tmpGame).getGLGraphics();
    }

}
