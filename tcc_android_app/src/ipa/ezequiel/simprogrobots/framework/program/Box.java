package ipa.ezequiel.simprogrobots.framework.program;

import java.util.ArrayList;
import java.util.List;

import ipa.ezequiel.simprogrobots.framework.Pixmap;

public class Box implements Cloneable{
	
	private int iIndex;
	private int posX;
	private int posY;
	private int iSize;
	private String sType;
	private EnumBoxes sTypeComp;
	private int iColor;	
	
	private List<Option> lstOptions;
	private List<String> lstValueOptions;
	
	
	protected Pixmap pImage;
	
	public Box( int index, int posX, int posY, int size, String type, int color, Pixmap image ){
		this.iIndex = index;
		this.posX = posX;
		this.posY = posY;
		this.setiSize(size);
		this.sType = type;
		this.iColor = color;
		this.pImage = image;
		this.sTypeComp = EnumBoxes.NOTHING;
		
		lstOptions = new ArrayList<Option>();
		lstValueOptions = new ArrayList<String>();
	}
	
	//Get and Set of Index
	public int getiIndex() {
		return iIndex;
	}
	public void setiIndex(int iIndex) {
		this.iIndex = iIndex;
	}
	
	//Get and Set of PosX
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	//Get and Set of PosY
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	public String getsType() {
		return sType;
	}
	
	
	//TODO: Modificar para adicionar os tipos de programacao.
	public void setsType(String sType) {
		this.sType = sType;
	}
	public int getiColor() {
		return iColor;
	}
	
	//Cor da Caixa.
	public void setiColor(int iColor) {
		this.iColor = iColor;
	}

	public int getiSize() {
		return iSize;
	}

	public void setiSize(int iSize) {
		this.iSize = iSize;
	}

	public Pixmap getImage() {
		return pImage;
	}

	public void setImage(Pixmap image) {
		this.pImage = image;
	}
	
	public void setOption( List<Option> options){
		this.lstOptions = options;
	}
	
	public void addOption( Option option ){
		lstOptions.add(option);
	}
	
	public List<Option> getOptions(){
		return lstOptions;
	}
	
	public Box clone(){
		try {
			return (Box) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

	public EnumBoxes getsTypeComp() {
		return sTypeComp;
	}

	public void setsTypeComp(EnumBoxes sTypeComp) {
		this.sTypeComp = sTypeComp;
	}

	public void clearOptions(){
		lstValueOptions.clear();
	}
	
	public List<String> getLstValueOptions() {
		return lstValueOptions;
	}

}
