package ipa.ezequiel.simprogrobots.framework.program;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.R.string;
import android.content.ClipData.Item;

public class CheckOption extends Option implements Cloneable {

	public class Items {
		public String  value;
		public boolean checked;
	}
	
	public int size;
	private List<Items> actualValues;
	
	public CheckOption( List<String> values ){
		
		super("Check");
		
		size = 0;
		actualValues = new ArrayList<Items>();
		
		Items novo;
		
		for( int i = 0; i < values.size(); i++ ){
			novo = new Items();
			novo.value = values.get(i);
			novo.checked = false;
			actualValues.add(novo);
			size++;
		}
	}
		
	public CheckOption clone()        throws CloneNotSupportedException{
		CheckOption opt = (CheckOption) super.clone();
		List<Items> copy = new ArrayList<Items>();
		for(int i = 0; i < actualValues.size() ; i ++){
			Items novo = new Items();
			novo.checked = actualValues.get(i).checked;
			novo.value =   actualValues.get(i).value;
			copy.add(novo);
		}
		opt.actualValues = copy;
		return opt;
	}

	public String valueItem(int i) {
		return actualValues.get(i).value;
	}
	public boolean checkedItem(int i){
		return actualValues.get(i).checked;
	}

	public void setCheckedItem(int i) {
		actualValues.get(i).checked = !actualValues.get(i).checked;
		
	}
}
