package ipa.ezequiel.simprogrobots.framework.program;

public class DegreeOption extends Option implements Cloneable {
	
	private float minValue;
	private float maxValue;
	private float actualValue;
	
	
	public DegreeOption(float d, float maxValue ){
		super("Degree");
		this.actualValue = 90f;
		this.minValue = d;
		this.maxValue = maxValue;
	}
	
	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public float getActualValue() {
		return actualValue;
	}

	public void setActualValue(float actualValue) {
		this.actualValue = actualValue;
	}
	
	public DegreeOption clone()
            throws CloneNotSupportedException{
		DegreeOption opt = (DegreeOption) super.clone();
		return opt;
	}
}
