package ipa.ezequiel.simprogrobots.framework.program;

public enum EnumBoxes {

	NOTHING,
	START,
	IF_INIT,
	IF_TRUE,
	IF_FALSE,
	IF_END,
	WHILE,
	WHILE_CONT,
	WHILE_END
}
