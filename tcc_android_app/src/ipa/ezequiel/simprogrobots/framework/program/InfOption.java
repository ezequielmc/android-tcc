package ipa.ezequiel.simprogrobots.framework.program;

public class InfOption extends Option implements Cloneable {

	private String actualValue;
	
	
		public InfOption( String value ){
			super("Information");
			this.setActualValue(value);
		}
		
		public InfOption clone()        throws CloneNotSupportedException{
			InfOption opt = (InfOption) super.clone();
			return opt;
		}

		public String getActualValue() {
			return actualValue;
		}
		public void setActualValue(String actualValue) {
			this.actualValue = actualValue;
		}
}
