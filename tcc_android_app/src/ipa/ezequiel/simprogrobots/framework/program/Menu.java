package ipa.ezequiel.simprogrobots.framework.program;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.graphics.Color;
import ipa.ezequiel.simprogrobots.Assets2D;
import ipa.ezequiel.simprogrobots.framework.program.Tab;

public class Menu {

	
	private List<Tab> lstTabs;
	
	int canvasWidth;
	int canvasHeigth;
	
	public Menu( int iCanvasWidth, int iCanvasHeigth){
		
		this.canvasHeigth = iCanvasHeigth;
		this.canvasWidth  = iCanvasWidth;
		int iPortion = iCanvasHeigth / 4 ;
		//Add the four tabs of the program
		//TODO: Modificar para um melhor caminho.
		lstTabs = new ArrayList<Tab>();
		/*
		lstTabs.add( new Tab( true,   (byte)1 , Color.BLUE , iCanvasWidth - 10 , 0 , iPortion) );
		lstTabs.add( new Tab( false , (byte)2 , Color.RED , iCanvasWidth - 10 , iPortion ,     iPortion  ));
		lstTabs.add( new Tab( false , (byte)3 , Color.GREEN , iCanvasWidth - 10, iPortion * 2, iPortion));
		lstTabs.add( new Tab( false , (byte)4 , Color.YELLOW , iCanvasWidth - 10 , iPortion * 3, iPortion));
		*/
		lstTabs.add( new Tab( true,   (byte)1 , Color.parseColor("#2C96DD") , iCanvasWidth - 10 , 0 , iPortion) );
		lstTabs.add( new Tab( false , (byte)2 , Color.parseColor("#E0942F") , iCanvasWidth - 10 , iPortion ,     iPortion  ));
		lstTabs.add( new Tab( false , (byte)3 , Color.parseColor("#D42323"), iCanvasWidth - 10, iPortion * 2, iPortion));
		lstTabs.add( new Tab( false , (byte)4 , Color.parseColor("#33E468") , iCanvasWidth - 10 , iPortion * 3, iPortion));
		
		createTabOptions();
		
	}
	
	private void createTabOptions(){
		
		
		/* CRIA MENU 1 */
		Tab tmpTab = lstTabs.get(0) ;
		
		Box tmp = new Box(1, canvasWidth - 102 , 0  , 72, "UP", Color.TRANSPARENT, Assets2D.imgFront);
		tmp.addOption(new StepsOption(1, 60));
		tmpTab.addLstBox( tmp );
		
		tmp = new Box(2, canvasWidth - 102 , 80  , 72, "DOWN", Color.TRANSPARENT, Assets2D.imgBack);
		/*
		 * tmp.addOption(new TimeOption( 0.5f, 10) );
		 * tmp.addOption(new VelocityOption(1, 10) );
		 */
		tmp.addOption(new StepsOption(1, 60));
		tmpTab.addLstBox( tmp );
		
		tmp = new Box(3, canvasWidth - 102 , 160 , 72, "LEFT", Color.TRANSPARENT, Assets2D.imgLeft);
		/*
		 * tmp.addOption(new TimeOption( 0.5f, 10) );
		 * tmp.addOption(new VelocityOption(1, 10) );
		 */
		tmp.addOption(new DegreeOption(1, 359));
		tmpTab.addLstBox( tmp );
		
		tmp = new Box(4, canvasWidth - 102 , 240 , 72, "RIGHT", Color.TRANSPARENT, Assets2D.imgRight);
		/*
		 * tmp.addOption(new TimeOption( 0.5f, 10) );
		 * tmp.addOption(new VelocityOption(1, 10) );
		 */
		tmp.addOption(new DegreeOption(1, 359));
		tmpTab.addLstBox( tmp );
		
		
		// Menu Times
		tmpTab = lstTabs.get(1) ;
		
		tmp = new Box(4, canvasWidth - 102 , 0 , 72, "TIMER", Color.TRANSPARENT, Assets2D.imgTimer);
		tmp.addOption(new TimeOption(1, 120));
		tmpTab.addLstBox( tmp );
		
		tmp = new Box(4, canvasWidth - 102 , 80 , 72, "IF", Color.TRANSPARENT, Assets2D.imgIF);
		tmpTab.addLstBox( tmp );
		
		tmp = new Box(4, canvasWidth - 102 , 160 , 72, "WHILE", Color.TRANSPARENT, Assets2D.imgWHILE);
		tmpTab.addLstBox( tmp );
		
		tmp = new Box(4, canvasWidth - 102 , 240 , 72, "ENDWHILE", Color.TRANSPARENT, Assets2D.imgEndWhile);
		tmpTab.addLstBox( tmp );
		
		//***************************************************************************************************/
		//***************************************************************************************************/
		//***************************************************************************************************/
		if( StoreProgram.getStoreProgram().getiRobot() == 1){
		
			tmpTab = lstTabs.get(2);
		
			//Nao Tem Opcoes
			tmp = new Box(900, canvasWidth - 102 , 0 , 72, "D_DIREITA", Color.TRANSPARENT, Assets2D.imgD_Dir);
			tmpTab.addLstBox( tmp );
			//Nao Tem Opcoes
			tmp = new Box(900, canvasWidth - 102 , 80 , 72, "D_ESQUERDA", Color.TRANSPARENT, Assets2D.imgD_Esq);
			tmpTab.addLstBox( tmp );
			//Nao Tem Opcoes
			tmp = new Box(900, canvasWidth - 102 , 160 , 72, "D_NORMAL", Color.TRANSPARENT, Assets2D.imgD_Normal);
			tmpTab.addLstBox( tmp );
			
			//Nao Tem Opcoes
			tmp = new Box(900, canvasWidth - 102 , 240 , 72, "S_DIST", Color.TRANSPARENT, Assets2D.imgS_Dist);
			tmp.addOption(new InfOption("Distancia ate o objeto: "));
			tmp.addOption(new StepsOption(1, 60));
			tmpTab.addLstBox( tmp );
			
			tmpTab = lstTabs.get(3);
			
			tmp = new Box(7, canvasWidth - 102 , 0 , 72, "MESSAGE", Color.TRANSPARENT, Assets2D.imgMessage);
			// Message
			tmpTab.addLstBox( tmp );

			tmp = new Box(6, canvasWidth - 102 , 80 , 72, "POS_LOCALE_DIST", Color.TRANSPARENT, Assets2D.imgPos_S_Dist);
			tmp.addOption(new DegreeOption(-90, 90));
			tmpTab.addLstBox( tmp );
		
			
		}
		else{
			
			tmpTab = lstTabs.get(2);
			
			tmp = new Box(900, canvasWidth - 102 , 0 , 72, "TOQUE", Color.TRANSPARENT, Assets2D.imgS_Toque);
			tmp.addOption(new CheckOption(Arrays.asList("Sensor Esquerda", "Sensor Direita")));
			tmpTab.addLstBox( tmp );
			
			tmp = new Box(6, canvasWidth - 102 , 80 , 72, "LED_ON", Color.TRANSPARENT, Assets2D.imgLedOn);
			tmp.addOption(new CheckOption(Arrays.asList("LED Esquerda", "LED Centro", "LED Direita")));
			tmpTab.addLstBox( tmp );
			
			tmp = new Box(6, canvasWidth - 102 , 160 , 72, "LED_OFF", Color.TRANSPARENT, Assets2D.imgLedOff);
			tmp.addOption(new CheckOption(Arrays.asList("LED Esquerda", "LED Centro", "LED Direita")));
			tmpTab.addLstBox( tmp );
		}
	}
	
	public int getSizeLstTabs(){
		return lstTabs.size();
	}
	
	public Tab getTab(int indexTab){
		return (Tab)lstTabs.get(indexTab);
	}
	
	public List<Box> getLstBox( int indexTab){
		return lstTabs.get(indexTab).getLstBox();
	}
}
