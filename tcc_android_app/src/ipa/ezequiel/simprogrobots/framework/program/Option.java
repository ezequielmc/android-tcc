package ipa.ezequiel.simprogrobots.framework.program;

public class Option implements Cloneable{

	private String tipo;

	public Option( String tipo ){
		this.tipo = tipo;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Option clone()
            throws CloneNotSupportedException{
		Option opt = (Option) super.clone();
		return opt;
	}
}
