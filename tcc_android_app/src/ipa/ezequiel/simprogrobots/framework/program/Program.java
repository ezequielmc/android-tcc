package ipa.ezequiel.simprogrobots.framework.program;

import java.util.LinkedList;

import min3d.vos.Number3d;
import android.util.Log;
import android.widget.ImageView;

public class Program {

	private LinkedList<Box> boxs;
	private boolean running;
	private Box executeBox;
	private float boxVelocity;
	private boolean finished = false;
	private float boxDegree;
	private float boxSteps;
	private float totalSteps;
	private float variation;
	private boolean isCompletedExecBox;
	private float totalRotation;
	private Number3d initialPosition;
	private Number3d initialRotation;
	private float totalExecution;
	private float boxTime;	
	
	@SuppressWarnings("unchecked")
	public Program(){
		
		startRunningProgram();
		running = false;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	@SuppressWarnings("unchecked")
	public void startRunningProgram(){
		boxs = (LinkedList<Box>) StoreProgram.getStoreProgram().getLstProgram().clone();
		running = true;
		setFinished(false);
	}
	
	private void setProperties(Number3d position, Number3d rotation){
		
		boxVelocity = 2.5f;
		for(int i = 0; i < executeBox.getOptions().size(); i++){
			switch ( executeBox.getOptions().get(i).getTipo() ) {
			//present time Option
			case "Time":
				boxTime = ( (TimeOption) executeBox.getOptions().get(i)).getActualValue();
				break;
			case "Velocity":
				boxVelocity = 2.5f; //( (VelocityOption) executeBox.getOptions().get(i)).getActualValue();
				break;
			case "Steps":
				boxSteps = ( (StepsOption) executeBox.getOptions().get(i)).getActualValue() ;
				break;
			case "Degree":
				boxVelocity = 18.0f;
				boxDegree = ( (DegreeOption) executeBox.getOptions().get(i)).getActualValue() ;
				break;
			}
		}
		
	}
	
	
	public guiChanges runProgram(float deltaTime, Number3d position, Number3d rotation, ImageView iViewExecution, 
			Number3d DistSensor){
		
		int resID  = ipa.ezequiel.simprogrobots.R.drawable.blank;
		//Se nao ha blocos de programacao na tela
		if( boxs.size() == 0 ){
			running = false;
			setFinished(true);  
			return new guiChanges(ipa.ezequiel.simprogrobots.R.drawable.blank, "Parado", false);
		}
	
		else if(executeBox == null || executeBox.getsType() == "SPACE" ){
			executeBox = boxs.get(0);
			boxs.remove(0);
			setProperties(position, rotation);
			isCompletedExecBox = true;
		}
		
		//Se o tempo de exec
		if( isCompletedExecBox ){
			executeBox = boxs.get(0);
			setProperties(position, rotation);
			boxs.remove(0);

			// Init Initial
			initialPosition = position.clone();
			initialRotation = rotation.clone();
			
			// Init Parameters
			totalSteps = 0f;
			totalRotation = 0f;
			totalExecution = 0f;
			isCompletedExecBox = false;
		}
		
		
		if( executeBox.getsType() == "UP" ){
			variation = -boxVelocity * deltaTime;
			totalSteps += variation;
			position.z += variation * Math.cos(Math.toRadians(rotation.y)) ;
			position.x += variation * Math.sin(Math.toRadians(rotation.y));
			
			resID = ipa.ezequiel.simprogrobots.R.drawable.up;
			if( totalSteps < boxSteps * - 1 ){
				position.z = (float) (initialPosition.z - boxSteps * Math.cos(Math.toRadians(rotation.y))) ;
				position.x = (float) (initialPosition.x - boxSteps * Math.sin(Math.toRadians(rotation.y))) ;
				isCompletedExecBox = true;
			}
		}
		else if( executeBox.getsType() == "DOWN" ){
			variation = boxVelocity * deltaTime ;
			totalSteps += variation ;
			position.z += variation * Math.cos(Math.toRadians(rotation.y)) ;
			position.x += variation * Math.sin(Math.toRadians(rotation.y)) ;
			
			resID = ipa.ezequiel.simprogrobots.R.drawable.up;
			if( totalSteps > boxSteps ){
				isCompletedExecBox = true;
				position.z = (float) (initialPosition.z + boxSteps * Math.cos(Math.toRadians(rotation.y)));
				position.x = (float) (initialPosition.x + boxSteps * Math.sin(Math.toRadians(rotation.y)));
				
			}
		}
		
		else if( executeBox.getsType() == "LEFT" ){
			variation = boxVelocity * deltaTime;
			totalRotation += variation;
			rotation.y += variation;
			
			
			resID = ipa.ezequiel.simprogrobots.R.drawable.left;
			if( totalRotation > boxDegree ){
				isCompletedExecBox = true;
				rotation.y = initialRotation.y + boxDegree; ;
			}

		}
		else if( executeBox.getsType() == "RIGHT" ){
			variation = boxVelocity * deltaTime;
			totalRotation -= variation;
			rotation.y -= variation;
			
			resID = ipa.ezequiel.simprogrobots.R.drawable.right;
			
			if( totalRotation <  boxDegree * -1){
				isCompletedExecBox = true;
				rotation.y = initialRotation.y - boxDegree;
			}
		}
		
		else if( executeBox.getsType() == "TIMER"){
			totalExecution += deltaTime;
			resID = ipa.ezequiel.simprogrobots.R.drawable.timer;
			if( totalExecution > boxTime){
				isCompletedExecBox = true;
			}
		}
		
		else if( executeBox.getsType() == "POS_LOCALE_DIST"){
			variation = boxVelocity * deltaTime;
			//ARRUMAR
			
			
			resID = ipa.ezequiel.simprogrobots.R.drawable.ps_dist;
			
			if( (boxDegree + ( DistSensor.y - rotation.y) ) < - 1 ){
				DistSensor.y += variation;
			
			}else if((boxDegree + (DistSensor.y- rotation.y) ) > 1){
				DistSensor.y -= variation;
			}
			else {
				DistSensor.y = -1*boxDegree ;
				isCompletedExecBox = true;
			}
			
		}

		//TODO: Modificar mensagem
		return new guiChanges(resID, "", true);
	} 
	
	public void stopProgram(){
		running = false;
	}

	public void run() {
		running = true;
	}

	public boolean isFinished() {
		return finished;
	}
	
	private void setFinished(boolean b) {
		this.finished = b;
	}

	
}
