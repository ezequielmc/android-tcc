package ipa.ezequiel.simprogrobots.framework.program;

public class StepsOption extends Option implements Cloneable {
	
	private float minValue;
	private float maxValue;
	private float actualValue;
	
	
	public StepsOption(float d, float maxValue ){
		super("Steps");
		this.actualValue = 5f;
		this.minValue = d;
		this.maxValue = maxValue;
	}
	
	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public float getActualValue() {
		return actualValue;
	}

	public void setActualValue(float actualValue) {
		this.actualValue = actualValue;
	}
	
	public StepsOption clone()
            throws CloneNotSupportedException{
		StepsOption opt = (StepsOption) super.clone();
		return opt;
	}
}
