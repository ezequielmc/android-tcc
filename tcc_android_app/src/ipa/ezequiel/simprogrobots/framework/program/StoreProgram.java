package ipa.ezequiel.simprogrobots.framework.program;

import java.util.LinkedList;

public class StoreProgram {

	private static StoreProgram reference;
	private LinkedList<Box> lstProgram;
	private int iRobot;
	private int iScenario;
	
	private StoreProgram(){
		setiRobot(1);
		setiScenario(1);
	}
	
	public static StoreProgram getStoreProgram(){
		if(reference == null)
			reference = new StoreProgram();
		return reference;
	}
	
	public void setLstProgram( LinkedList<Box> lst ){
		this.lstProgram = lst;
	}
	
	public LinkedList<Box> getLstProgram( ){
		return this.lstProgram;
	}


	
	
	public int getiRobot() {
		return iRobot;
	}

	public void setiRobot(int iRobot) {
		this.iRobot = iRobot;
	}

	public int getiScenario() {
		return iScenario;
	}

	public void setiScenario(int iScenario) {
		this.iScenario = iScenario;
	}

}
