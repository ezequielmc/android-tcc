package ipa.ezequiel.simprogrobots.framework.program;

import java.util.ArrayList;
import java.util.List;

public class Tab {

	private boolean isSelected;
	private byte bIndex;
	private int iColor;
	private List<Box> lstBox;
	private int iPosX;
	private int iPosY;
	private int iMaxSize;
	
	public Tab( boolean isSelected, byte bIndex, int iColor, int posX, int posY , int maxSize){

		this.isSelected = isSelected;
		this.bIndex = bIndex;
		this.iColor = iColor;
		this.setiPosX(posX);
		this.setiPosY(posY);
		this.setiMaxSize(maxSize);
		lstBox = new ArrayList<Box>();
	
	}
	
	//Get Index
	public byte getbIndex() {
		return bIndex;
	}
	public void setbIndex(byte bIndex) {
		this.bIndex = bIndex;
	}
	
	public int getiColor() {
		return iColor;
	}
	public void setiColor(int iColor) {
		this.iColor = iColor;
	}
	
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public List<Box> getLstBox() {
		return lstBox;
	}
	
	public int sizeLstBox(){
		return lstBox.size();
	}
	
	public Box getBox( int index ){
		return lstBox.get( index );
	}
	
	public void addLstBox( Box addBox) {
		this.lstBox.add(addBox);
	}

	public int getiPosX() {
		return iPosX;
	}

	public void setiPosX(int iPosX) {
		this.iPosX = iPosX;
	}

	public int getiPosY() {
		return iPosY;
	}

	public void setiPosY(int iPosY) {
		this.iPosY = iPosY;
	}

	public int getiMaxSize() {
		return iMaxSize;
	}

	public void setiMaxSize(int iMaxSize) {
		this.iMaxSize = iMaxSize;
	}
	
}
