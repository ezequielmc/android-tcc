package ipa.ezequiel.simprogrobots.framework.program;

public class TimeOption extends Option implements Cloneable {
	
	private float minValue;
	private float maxValue;
	private float actualValue;
	
	
	public TimeOption(float d, float maxValue ){
		super("Time");
		this.actualValue = 5f;
		this.minValue = d;
		this.maxValue = maxValue;
	}
	
	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public float getActualValue() {
		return actualValue;
	}

	public void setActualValue(float actualValue) {
		this.actualValue = actualValue;
	}
	
	public TimeOption clone()
            throws CloneNotSupportedException{
		TimeOption opt = (TimeOption) super.clone();
		return opt;
	}
}
