package ipa.ezequiel.simprogrobots.framework.program;

public class VelocityOption extends Option {
	
	private float maxValue;
	private float minValue;
	private float actualValue;
	
	public VelocityOption(float minValue, float maxValue){
		super("Velocity");
		this.setActualValue(1);
		this.setMinValue(minValue);
		this.setMaxValue(maxValue);
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public float getActualValue() {
		return actualValue;
	}

	public void setActualValue(float actualValue) {
		this.actualValue = actualValue;
	}
	
	public VelocityOption clone()
            throws CloneNotSupportedException{
		VelocityOption opt = (VelocityOption) super.clone();
		return opt;
	}
}
