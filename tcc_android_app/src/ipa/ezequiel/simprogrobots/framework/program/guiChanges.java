package ipa.ezequiel.simprogrobots.framework.program;

public class guiChanges {

	private int resID;
	private boolean isRunning;
	private String value1;
	private String value2;
	private String value3;
	private String value4;
	
	
	public guiChanges(int resID, String first, boolean isRunning){
		this.resID = resID;
		this.value1 = first;
		this.setRunning(isRunning);
	}
	
	public int getResID() {
		return resID;
	}
	public void setResID(int resID) {
		this.resID = resID;
	}
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	public String getValue3() {
		return value3;
	}
	public void setValue3(String value3) {
		this.value3 = value3;
	}
	public String getValue4() {
		return value4;
	}
	public void setValue4(String value4) {
		this.value4 = value4;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}
	
	
}
