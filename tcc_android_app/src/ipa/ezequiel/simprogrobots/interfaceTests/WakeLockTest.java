package ipa.ezequiel.simprogrobots.interfaceTests;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

@SuppressLint("Registered")
public class WakeLockTest extends FullScreenTest {
    WakeLock wakeLock;
    
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Lock");        
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public void onResume() {
        wakeLock.acquire();
        super.onResume();
    }
    
    public void onPause() {
        wakeLock.release();
        super.onPause();
    }
}
