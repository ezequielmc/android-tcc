package min3d.objectPrimitives;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import min3d.Shared;
import min3d.Utils;
import min3d.core.Object3dContainer;
import min3d.vos.Color4;

public class SkyRect extends Object3dContainer {
	private float size;
	private float sizeHeight;
	private float sizeWidth;
	private int quality;
	private Color4 color;
	private Rectangle[] faces;
	private float halfSizeW;
	private float halfSizeH;
	private float halfSize;
	
	private static Resources res;
	
	public enum Face {
		North,
		East,
		South,
		West,
		Up,
		Down,
		All
	}
	
	public SkyRect(Resources resources, float width, float height, float size, int quality) {
		super(0, 0);
		this.size = size;
		this.sizeHeight = height;
		this.sizeWidth = width;
		this.halfSizeH = sizeHeight *.5f;
		this.halfSizeW = sizeHeight *.5f;
		this.halfSize = size * .5f;
		this.quality = quality;
		SkyRect.res = resources;
		build();
	}
	
	private void build() {
		color = new Color4();
		faces = new Rectangle[6];
		Rectangle north = new Rectangle(sizeWidth, size, quality, quality, color);
		Rectangle east = new Rectangle(sizeHeight, size, quality, quality, color);
		Rectangle south = new Rectangle(sizeWidth, size, quality, quality, color);
		Rectangle west = new Rectangle(sizeHeight, size, quality, quality, color);
		Rectangle up = new Rectangle(sizeWidth, sizeHeight, quality, quality, color);
		Rectangle down = new Rectangle(sizeWidth, sizeHeight, quality, quality, color);
		
		north.position().z = halfSizeH;
		north.lightingEnabled(false);
		
		east.rotation().y = -90;
		east.position().x = halfSizeW*.72f;
		east.doubleSidedEnabled(true);
		east.lightingEnabled(false);
		
		south.rotation().y = 180;
		south.position().z = -halfSizeH;
		south.lightingEnabled(false);
		
		west.rotation().y = 90;
		west.position().x = -halfSizeW*.72f;
		west.doubleSidedEnabled(true);
		west.lightingEnabled(false);
		
		up.rotation().x = 90;
		up.position().y = halfSize * 90;
		up.doubleSidedEnabled(true);
		up.lightingEnabled(false);
		
		down.rotation().x = -90;
		down.position().y = -halfSize;
		down.doubleSidedEnabled(true);
		down.lightingEnabled(false);
		
		faces[Face.North.ordinal()] = north;
		faces[Face.East.ordinal()] = east;
		faces[Face.South.ordinal()] = south;
		faces[Face.West.ordinal()] = west;
		faces[Face.Up.ordinal()] = up;
		faces[Face.Down.ordinal()] = down;
		
		addChild(north);
		addChild(east);
		addChild(south);
		addChild(west);
		addChild(up);
		addChild(down);
	}
	
	
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / 2;
        final int halfWidth = width / 2;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }

    return inSampleSize;
	}
	
	
	public static Bitmap decodeSampledBitmapFromResource(int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true; 
	    BitmapFactory.decodeResource(res, resId, options);
 
	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	public void addTexture(Face face, int resourceId, String id) {
		
		Bitmap bitmap = Utils.makeBitmapFromResourceId(resourceId);  
		//Bitmap bitmap = decodeSampledBitmapFromResource(resourceId, 1680,2400);
		 
		Shared.textureManager().addTextureId(bitmap, id, true);
		bitmap.recycle();
		addTexture(face, bitmap, id);  
		System.gc(); 
	}
	
	public void addTexture(Face face, Bitmap bitmap, String id) {
		if(face == Face.All)
		{
			for(int i=0; i<6; i++)
			{
				faces[i].textures().addById(id);
			}
		}
		else
		{
			faces[face.ordinal()].textures().addById(id);
		}
	}

}
